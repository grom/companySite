<%@page import="com.grom.util.Constants"%>
<%@page import="com.grom.po.Customer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/themes/icon.css">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.easyui.min.js"></script>

<script type="text/javascript">
var currentUserId = "";
currentUserId="ADMIN";

$(function(){
	$('#loginInfoEdit').dialog({  
	    title : 'Customer',  
	    modal: true,  
	    collapsible : true,    
	    resizable : true   
	});  
	$('#loginInfoEdit').dialog('close');
	$('#uploadWin').window('close');
});


function showAddDialog(){
	$('input').each(function(){
		$(this).val('');
	});
	$('select').each(function(){
		$(this).val('');
	});
	$('#save').linkbutton('enable');
	$('#loginInfoEdit').dialog('open');
}

function loginInfoEdit(){
	var canSub = true;
	$('input').each(function(){
		if($(this).attr("required")=="true"){
			if($(this).val()==""){
				$(this).focus();
				canSub =false;
				return;
			}
		}
	});
	$('select').each(function(){
		if($(this).attr("required")=="true"){
			if($(this).val()==""){
				$(this).focus();
				canSub =false;
				return;
			}
		}
	});
	if(canSub){
		$('#CustomerForm').submit();
	}
}
function viewDetail(){
	var itemId =  $('#tt').datagrid('getSelected').itemid;
	$.ajax({
	    url: '<%=request.getContextPath()%>/CustomerAction/getDetail.do?itemid='+itemId,
	    type: 'GET',
	    timeout: 1000,
	    error: function(){
	        alert('Error getting response!');
	    },
	    success: function(json){
	        var Customer = eval(json);
							$('#ID').val(Customer.ID);
							$('#Cname').val(Customer.Cname);
							$('#Ename').val(Customer.Ename);
							$('#url').val(Customer.url);
							$('#logo').val(Customer.logo);
						$('#loginInfoEdit').dialog('open');
	    }
	});
}

function doDelete(){
	var itemId =  $('#tt').datagrid('getSelected').itemid;
	if (itemId){  
        $.messager.confirm('Confirm','Are you sure you want to remove this user?',function(r){  
            if (r){  
				$('#deleteId').val(itemId);
				$('#deleteForm').submit();
            }  
        });  
    } 
}
function doSearch(){
	$('#searchForm').submit();
}
function showUpload(){
	$('#uploadFrame').attr("src","<%=request.getContextPath() %>/ImportAction/gotoImportPic.do");
	$('#uploadWin').window('open');
}

function showPic(){
	var pic =  $("#logo").val();
	$("#picImg").attr("src","<%=request.getContextPath() %>/static/pic/"+pic);
}
</script>
</head>
<body>
<div style="display: none;">
	<form name="deleteForm" id="deleteForm" action="<%=request.getContextPath() %>/CustomerAction/doDelete.do">
		<input type="text" name="deleteId" id="deleteId"/>
	</form>
</div>
<div style="font-size: 10px;">
	<form id="searchForm" name="searchForm" action="<%=request.getContextPath() %>/CustomerAction/conditionSelect.do" method="post">
      <label>condition1:</label>  
      <input type="text" name="clothType_s" id="clothType_s" ></input>  
      <label>condition2:</label>  
      <input type="text" name="brandName_s" id="brandName_s" ></input>  
      <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch()">search</a>
    </form>
</div>
<table id="tt" class="easyui-datagrid"  singleSelect="true"
        title="DataGrid with Toolbar" iconCls="icon-save"  
        toolbar="#tb">  
    <thead>  
        <tr>  
			<th field="itemid" width="80" hidden="true">Item ID</th>
							<th field="Cname" width="80" >Cname</th>
							<th field="logo" width="80" >Logo</th>
							<th field="Ename" width="80" >Ename</th>
							<th field="ID" width="80" >ID</th>
							<th field="url" width="80" >Url</th>
			        </tr>  
    </thead>  
    <tbody>     
    	<%
    		List<Customer> allCustomers = (List<Customer>)request.getAttribute("allCustomers");
    		for(Customer instance : allCustomers){
    	%>
        <tr> 
			<td><%=instance.getID() %></td> 
							<td><%=instance.getCname() %></td>
							<td><%=instance.getLogo() %></td>
							<td><%=instance.getEname() %></td>
							<td><%=instance.getID() %></td>
							<td><%=instance.getUrl() %></td>
			        </tr>  
    	<%
    		}
    	%>                         
    </tbody>
</table>  
<div id="tb">  
    <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="showAddDialog()">Add</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-cut" plain="true" onclick="viewDetail()">view/update</a>  
    <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="doDelete()">Delete</a> 
</div>  


<div id="loginInfoEdit"  
    style="padding: 5px; width: 600px; height: 450px;">  
    <h5 id="loginInfoEdit_message" style="color: red;"></h5>  
    <div class="ToolTip_Form" id="table_loginInfoEdit" onkeydown="if(event.keyCode==13){loginInfoEdit();}">  
       <form id="CustomerForm" name="CustomerForm" action="<%=request.getContextPath() %>/CustomerAction/doSave.do" method="post">
       <ul>    
											<li>  
					<label>ID:</label>  
					<input type="hidden" class="easyui-validatebox" name="ID" id="ID"  ></input>  
				</li>
										
												<li>  
						<label>Cname:</label>  
						<input type="text" class="easyui-validatebox" name="Cname" id="Cname" required="true"></input>  
					</li>	
										
												<li>  
						<label>Ename:</label>  
						<input type="text" class="easyui-validatebox" name="Ename" id="Ename" required="true"></input>  
					</li>	
										
												<li>  
						<label>url:</label>  
						<input type="text" class="easyui-validatebox" name="url" id="url" required="true"></input>  
					</li>	
										
												<li>  
						<label>logo:</label>  
						<input type="text" class="easyui-validatebox" name="logo" id="logo" required="true"></input>  
						<a id="upload" name=""upload"" href="#" class="easyui-linkbutton" onclick="showUpload();">upload</a>
					</li>	
										
			            <li>  
             	<a id="save" name="save" href="#" class="easyui-linkbutton" onclick="loginInfoEdit();">Save</a>
            	<a href="#" class="easyui-linkbutton" onclick="$('#loginInfoEdit').dialog('close');">Close</a>
            	<a href="#" class="easyui-linkbutton" onclick="showPic()">showPic</a>
            	<br>
            	<img id="picImg" style="width: 40px; height: 30px;" src=""/>
            </li>  
        </ul>  
       </form>
    </div>  
</div>  
<div id="uploadWin" class="easyui-window" title="uploadWin" style="width:500px;height:300px;padding:5px;">  
    <iframe id="uploadFrame" src="<%=request.getContextPath() %>/ImportAction/gotoImport.do" style="border: none;"></iframe>
</div>  
</body>
</html>