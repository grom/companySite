﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="com.grom.service.CustomerService"%>
<%@page import="com.grom.po.Customer"%>
<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">  
<head> 
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />  
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/static/coin-slider.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/static/styles.css" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/static/coin-slider-styles.css" type="text/css">
<style type="text/css">
*{padding:0;margin:0;text-align:center;}
body{
	background-attachment: scroll;
	background-image: url(<%=request.getContextPath() %>/static/i/main_bg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	text-align:center;
	font-size:12px;
	font-family: "宋体", Arial;
}
a{
	font:bold 12px Verdana;
	color:#007153;
	text-decoration:none;
}
#wrapper{
	background-image: url(<%=request.getContextPath() %>/static/i/bgbg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	width:971px;
	height:700px;
	margin:0 auto;
}
#header{
	text-align:left;
	padding-left:50px;
	font-size:40px;
	color:#E7EBEA;
	height:75px;
}
#header a{
	color:#E7EBEA;
	font:12px normal;
}
#logo{
	display:block;
	position:relative;
	padding-top:20px;
	text-align:left;
}
#navigator{
	position:relative;
	font:bold 14px Verdana;
	text-align:right;
	padding-left:50px;
}
#navigator ul{
	list-style-image:none;
	list-style-type:none;
}
#navigator ul li{
	display:block;
	float:left;
	line-height:21px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background:url(<%=request.getContextPath() %>/static/i/nav1-line.gif) no-repeat 0px 0px;
	height:21px;
	width:90px;
}
#navigator ul li div{
	position:relative;
	display:none;
	margin-top:8px;
	width:700px;
	text-align:left;
	height:35px;
	z-index:10;
}
#navigator ul li:hover div{
	position:relative;
	display:block;
	margin-left:-15px;
	z-index:10;
}
#navigator ul li div span{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_bg.jpg) repeat-x 0px 0px;
	padding-left:5px;
	padding-right:5px;
	padding-top:3px;
	margin:0;
	float:left;
	height:40px;
}
#navigator ul li div span a{
	display:block;
	background:url(<%=request.getContextPath() %>/static/i/sub_menu_normal.jpg) no-repeat;
	width:110px;
	height:30px;
	padding-top:5px;
}
.begin{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_lbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
.end{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_rbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
#navigator ul li:hover{
	display:block;
	float:left;
	line-height:29px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background-color:#007153;
	height:21px;
}
#navigator ul li:hover a{
	color:#E7EBEA;
}
#first{
	background:url(<%=request.getContextPath() %>/static/i/nav1-left.gif) no-repeat 0px 0px;
}
#blank{
	position:relative;
	margin-top:30px;
	width:971px;
	height:30px;
	background-color:#E7EBEA;
}
#pov{
	position:relative;
	width:771px;
	height:315px;
	background:url("<%=request.getContextPath() %>/static/i/service/cangchu.jpg") no-repeat -50px -200px;
	float:left;
}
#list{
	position:relative;
	margin-left:775px;
	padding-top:5px;
	height:300px;
}
#list span{
	position:relative;
	display:block;
	width:185px;
	height:25px;
	background:url(<%=request.getContextPath() %>/static/i/shube.gif) no-repeat 0px 0px;
	font-size:15px;
	margin-top:2px;
	text-align: left;
	padding-left:10px;
}
#list span a{
	font:20px bold;
	color:black;
}
#list span a:hover{
	font:20px bold;
	color:#007153;
}
#modules{
	position:relative;
	margin-top:32px;
	width:971px;
	height:180px;
	background:url(<%=request.getContextPath() %>/static/i/bd.jpg) no-repeat 0px 0px;
	text-align:left;
}
#modules span{
	display:block;
	width:242px;
	height:40px;
	font-size:20px;
	padding-top:15px;
	color:black;
	float:left;
}
#modules div{
	text-align:left;
	display:none;
}
#modules div p{
	text-align:left;
	line-height:16px;
	font-size:12px;
}
#footer{
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/templatemo_bg.jpg) repeat ;
}
#modules div a{
	color:black;
	font:12px normal;
	line-height:16px;
}
</style>
<script type="text/javascript">
$(function(){
    $('#coin-slider').coinslider({ width: 800,height:315, navigation: false, delay: 5000 ,hoverPause: true,navigation: true});
	$("#cangchu").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-1").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	$("#huodai").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-2").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	$("#gongyinglian").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-3").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	$("#wuliuguihua").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-4").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	$("#yunshu").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-5").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	$("#xinxihua").mouseover(function(){
		rebackBack();
		$("#cs-button-coin-slider-6").trigger("click");
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shubea.gif) no-repeat 0px 0px");
	});
	
	$("#modules span").each(function(){
		$(this).mouseover(function(){
			$(this).css("background","url(<%=request.getContextPath() %>/static/i/xinb.jpg) no-repeat");
			$("#modules div").each(function(){
				$(this).css("display","none");
			});
			$("#"+$(this).attr("id")+"Div").css("display","block");
		});
		$(this).mouseout(function(){
			$(this).css("background","");
		});
	});
	
	$("#aboutusDiv").css("display","block");;
});

function rebackBack(){
	$("#list span").each(function(){
		$(this).css("background","url(<%=request.getContextPath() %>/static/i/shube.gif) no-repeat 0px 0px");
	});
}
</script>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<span id="logo">凡越物流</span>
		<div align="left" style="font-size: 10px; color: #FFF;position:relative;left:400px;top:-55px;">
            <p align="left" style="font-size:10px;color:#FFF">
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=zh-cn">中文</a> | 
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=eng">Eng</a>
			</p>
		</div>
	</div>
	<div id="navigator">
		<ul>
			<li class="first">
				<a href="<%=request.getContextPath() %>/DirectionAction/gotoHomePage.do">首页</a>
			</li>
			<%
				List<RootMenu> rootMenus = (List<RootMenu>)request.getAttribute("rootMenus");
	    		Map<String, List<SecondMenu>> childMenus = (Map<String, List<SecondMenu>>)request.getAttribute("childMenus");
	    		int length = rootMenus.size();
	    		for(int i = 0 ; i < length ; i++){
	    	%>
	    	<li class="first">
				<a href="#"><%=rootMenus.get(i).getName() %></a>
				<div>
					<tt class="begin"></tt>
					<%
						List<SecondMenu> thisChildren = childMenus.get(rootMenus.get(i).getID());
						int ch_len = thisChildren.size();
	            		for(int l = 0; l < ch_len ; l++){
	            	%>
		            	<span>
		            		<a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>">
		            			<%=thisChildren.get(l).getName() %>
		            		</a>
		            	</span> 
	            	<%
	            		}
					%>
					<tt class="end"></tt>
				</div>
			</li>
	    	<%
	    		}
			%>
			<li class="first">
				<a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=OTHERS">联系我们</a>
			</li>
		</ul>
	</div>
	<div id="blank">
		
	</div>
	<div id="pov">
		<div id='coin-slider'>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=cangchu">
				<img src='<%=request.getContextPath() %>/service/cangchu.jpg' >
				<span>
					D仓储
				</span>
			</a>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=huodai">
				<img src='<%=request.getContextPath() %>/service/huodai.jpg' >
				<span>
					货代
				</span>
			</a>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=gongyinglian">
				<img src='<%=request.getContextPath() %>/service/gongyinglian.jpg' >
				<span>
					供应链
				</span>
			</a>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=guihua">
				<img src='<%=request.getContextPath() %>/service/wuliuguihua.jpg' >
				<span>
					物流规划
				</span>
			</a>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=yunshu">
				<img src='<%=request.getContextPath() %>/service/yunshu.jpg' >
				<span>
					物流运输
				</span>
			</a>
			<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=xinxihua">
				<img src='<%=request.getContextPath() %>/service/xinxihua.jpg' >
				<span>
					信息化
				</span>
			</a>
		</div>
	</div>
	<div id="list">
		<span id="cangchu"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=cangchu">仓储</a></span>
		<span id="huodai"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=huodai">货代</a></span>
		<span id="gongyinglian"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=gongyinglian">供应链</a></span>
		<span id="wuliuguihua"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=guihua">物流规划</a></span>
		<span id="yunshu"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=yunshu">运输</a></span>
		<span id="xinxihua"><a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=xinxihua">信息化</a></span>
	</div>
	<div id="modules">
		<span id="aboutus">关于我们</span>
		<span id="demo">典型案例</span>
		<span id="news">新闻公告</span>
		<span id="services">行业热点</span>
		<div id="aboutusDiv">
			<p>&nbsp;&nbsp;&nbsp;&nbsp;上海凡越供应链服务有限公司成立于2004年，总部设立在上海。是一家提供“专业第三方物流”服务的内资企业。在国内工程机械行业先行导入“现代物流一体化管理”服务外包理念，为国内大型制造企业提供专业的运输、仓储、拆零、分拣、包装、配送等多项服务，并针对问题系统提供系统解决方案。</p>
    		<p>&nbsp;&nbsp;&nbsp;&nbsp;凡越运用“一体化管理”这一外包服务理念，为客户提供专业的物流服务新模式。公司顺应企业非核心业务外包的趋势，与客户一起进行企业流程再造，成为客户真正意义上的战略合作伙伴。本着“着眼未来、想企业之所需，解客户之所急”的原则，为大型企业提供具有特色的“采购、生产、销售物流一体化管理服务”，支持了企业的精益生产与精益运行；帮助客户降低管理成本，提高企业核心业务的市场竞争能力。为更高效的服务于客户，我们正在与知名的物流IT企业合作，开发、完善物流信息系统，以确保信息流的透明畅通。</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;凡越已通过ISO9001:2000质量管理体系认证。公司的管理服务理念与模式得到了客户、同行专家的认同和高度评价。我们将努力打造成为国内大型制造企业的供应链服务专家。</p>
		</div>
		<div id="demoDiv">
			<%
			List<Customer> allCustomers = CustomerService.instance.getAllCustomers();
			for(Customer cus : allCustomers){
				%>
					<span>
						<img width="40px" height="20px" src="<%=request.getContextPath() %>/static/pic/<%=cus.getLogo() %>"/>
						<a href="<%=cus.getUrl() %>">
						<%
							if(session.getAttribute("lan").equals("eng")){
								out.print(cus.getEname());
							}else{
								out.print(cus.getCname());
							}
						%>
						</a>
					</span>
				<%
			}
		%>
		</div>
		<div id="newsDiv">
			<a href="#">年会合照  2/2</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#">广西国际物流成立  2/2</a><br>
			<a href="#">上海凡越物流有限公司桐庐游记  6/19</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#">凡越文化核心  1/20</a><br>
			<a href="#">中石油华东地区考察团一行来访凡..  11/6</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#">我司全国免费服务热线即日起正式..  11/6</a><br>
			<a href="#">凡越物流全体员工为汶川灾区踊跃..  5/26</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#">通 报 表 扬  4/30</a><br>
		</div>
		<div id="servicesDiv">
			需要手动增加
		</div>
	</div>
	<div id="footer">
		copyright@fanyue
	</div>
</div>
</body>
</html>