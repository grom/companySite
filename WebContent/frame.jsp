<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/js/themes/icon.css">
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/frame.css">
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.6.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.easyui.min.js"></script>
<title>MainFrame</title>
<style type="text/css">
#topline{
	background-image: url(i/background.jsp) repeat-y;
}
</style>
</head>
<body>
<div id="topLine">
	欢迎使用后台管理
</div>
<div id="systemManagement" class="easyui-layout" style="width:100%;height:600px;">  
    <div id="title_sys" name="title_sys" region="west" split="true" title="系统管理" style="width:150px;">  
        <div id="saleDepartment" name="saleDepartment" class="easyui-accordion" fit="true" border="false" style="width:145px;height:550px;">  
	    	<div title="导航菜单管理" iconCls="icon-ok" style="padding:1px;">  
	        	<p><a href="<%=request.getContextPath() %>/TopMenuAction/gotoList.do" target="main">一级菜单</a></p>
	        	<p><a href="<%=request.getContextPath() %>/ChildMenuAction/gotoList.do" target="main">二级菜单</a></p>
	    	</div>  
	    	<div title="成功案例管理" iconCls="icon-ok" style="overflow:auto;padding:1px;">  
	        	<p><a href="<%=request.getContextPath() %>/CustomerAction/gotoList.do" target="main">成功案例</a></p>
	    	</div>  
	    	<div title="系统帮助" iconCls="icon-ok" style="overflow:auto;padding:1px;">  
	        	<p><a href="<%=request.getContextPath() %>/CustomerAction/gotoQuery.do" target="main">系统帮助</a></p>
	    	</div>  
	    	<div title="About system" iconCls="icon-reload" selected="true" style="padding:10px;">  
	        	 made by Grom
	    	</div>   
		</div>  
    </div>  
    <div id="content" region="center" title="操作区" style="padding:5px;">  
    	<iframe name="main" id="main" frameBorder="0" style="border: none;" width="100%" height="100%"></iframe>
    </div>  
</div>  
<div id="footer">
	copyright @ www.fanyue.cn
</div>
</body>
</html>