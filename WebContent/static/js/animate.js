﻿(
function($) {
    var defaults = {
        startIndex: 1,
        leftCircle: ".main_content .circle_left",
        rightCircle: ".main_content .circle_right",
        centerCircle: ".main_content .circle_center",
        productImags: ["static/i/canchu.png", "static/i/gongyinglian.png", "static/i/guihua.png",
        "static/i/yunshu.png", "static/i/xinxihua.png", "static/i/huodai.png"],
        speed: 1000
    };

    function SlideAnimate(options) {
        var settings = $.extend({}, defaults, options);
        var imageCount = settings.productImags.length;
        var startIndex = settings.startIndex;
        var speed = settings.speed;


        var init = function() {
            $(settings.leftCircle + " img").attr("src", settings.productImags[(startIndex) % imageCount]);
            $(settings.centerCircle + " img").attr("src", settings.productImags[(startIndex + 1) % imageCount]);
            $(settings.rightCircle + " img").attr("src", settings.productImags[(startIndex + 2) % imageCount]);
        }
        
        var changeComments = function(){
        	//$("#comments").html(commentId+ "<br>" + startIndex);
        	if(startIndex==0){
        		$("#comments").html("<p>供应链物流 : </P><a href='"+settings.urlStart+"/DirectionAction/gotoService.do?id=gongyinglian'>供应链物流指产品或服务从提供起点至消费终点及相关信息有效流动的全过程，包括采购、运输、配送、仓储、装卸..</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=gongyinglian");
        	}
        	if(startIndex==1){
        		$("#comments").html("<p>物流资源系统规划服务 : </P><a href='"+settings.urlStart+"/DirectionAction/gotoService.do?id=guihua'>点击查看详细</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=guihua");
        	}
        	if(startIndex==2){
        		$("#comments").html("<p>运输与生产配送服务 </P><a href='"+settings.urlStart+"/DirectionAction/gotoService.do?id=yunshu'>我们的运输配送方式涵盖：公路、铁路、水运以及航空运输。。</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=yunshu");
        	}
        	if(startIndex==3){
        		$("#comments").html("<p>物流信息化 : </P><a href='"+settings.urlStart+"/DirectionAction/gotoService.do?id=xinxihua'>我们把客户的物流活动和物流信息结合成一个有机的系统，通过各种信息技术，如条形码技术等，来实现物流信息化</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=xinxihua");
        	}
        	if(startIndex==4){
        		$("#comments").html("<p>采购供应物流服务 : </P><a href='"+settings.urlStart+"/DirectionAction/gotoService.do?id=huodai'>	分析客户现有的采购模式	研究针对客户的科学采购方式	帮助客户分析供应市场</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=huodai");
        	}
        	if(startIndex==5){
        		$("#comments").html("<p>仓储服务 : </P><a href="+settings.urlStart+"/"+"'DirectionAction/gotoService.do?id=cangchu'>我们的目的是：提高仓库的利用率和生产效率，充分利用仓储空间..</a>");
        		$("#urlShow").find("a").attr("href",settings.urlStart+"/DirectionAction/gotoService.do?id=cangchu");
        	}
        }


        var clickRight = function() {

            var $tempLeft = $(settings.leftCircle);
            var $tempCenter = $(settings.centerCircle);
            var $tempRight = $(settings.rightCircle);
            startIndex = (startIndex + 1) % imageCount;


            //Center circle to left
            $(settings.centerCircle).animate({ top: $tempLeft.css("top"), left: $tempLeft.css("left")
            },
            speed);

            $(settings.centerCircle + " img").animate({
                width: $tempLeft.find("img").css("width"), height: $tempLeft.find("img").css("height")
            },
            speed, function() {
                $(settings.centerCircle).attr("class", "circle_left");
                $(settings.leftCircle).unbind();
                $(settings.leftCircle).click(clickLeft);
            });
            //Right circle to center
            $(settings.rightCircle).animate({ top: $tempCenter.css("top"), left: $tempCenter.css("left")
            },
            speed);


            $(settings.rightCircle + " img").animate({
                width: $tempCenter.find("img").css("width"), height: $tempCenter.find("img").css("height")
            },
            speed, function() {
                $(settings.rightCircle).attr("class", "circle_center");
                $(settings.centerCircle).unbind();
            });


            //Right circle animation
            $(settings.leftCircle).remove();
            var rightDiv = "<div class='circle_right_animate'><a><img width='0'  height='0'/></a></div>";
            $(settings.centerCircle).parent().append(rightDiv);
            $(".circle_right_animate img").attr("src", settings.productImags[(startIndex + 2) % imageCount]);
            $(".circle_right_animate img").animate({
                width: $tempRight.find("img").css("width"), height: $tempRight.find("img").css("height")
            }, speed, function() {
                $(".circle_right_animate").attr("class", "circle_right");
                $(settings.rightCircle).unbind();
                $(settings.rightCircle).click(clickRight);
            });
            changeComments();
        }


        var clickLeft = function() {

            var $tempLeft = $(settings.leftCircle);
            var $tempCenter = $(settings.centerCircle);
            var $tempRight = $(settings.rightCircle);
            startIndex = ((startIndex - 1) % imageCount) == -1 ? imageCount - 1 : (startIndex - 1) % imageCount;



            //Center circle to Right
            $(settings.centerCircle).animate({ top: $tempRight.css("top"), left: $tempRight.css("left")
            },
            speed);


            $(settings.centerCircle + " img").animate({
                width: $tempRight.find("img").css("width"), height: $tempRight.find("img").css("height")
            },
            speed, function() {
                $(settings.centerCircle).attr("class", "circle_right");
                $(settings.rightCircle).unbind();
                $(settings.rightCircle).click(clickRight);
            });
            //Left circle to center
            $(settings.leftCircle).animate({ top: $tempCenter.css("top"), left: $tempCenter.css("left")
            },
            speed);


            $(settings.leftCircle + " img").animate({
                width: $tempCenter.find("img").css("width"), height: $tempCenter.find("img").css("height")
            },
            speed, function() {
                $(settings.leftCircle).attr("class", "circle_center");
                $(settings.centerCircle).unbind();
            });


            //Left circle animation
            $(settings.rightCircle).remove();
            var leftDiv = "<div class='circle_left_animate'><a><img width='0'  height='0'/></a></div>";
            $(settings.centerCircle).parent().append(leftDiv);
            $(".circle_left_animate img").attr("src", settings.productImags[startIndex % imageCount]);
            $(".circle_left_animate img").animate({
                width: $tempLeft.find("img").css("width"), height: $tempLeft.find("img").css("height")
            }, speed, function() {
                $(".circle_left_animate").attr("class", "circle_left");
                $(settings.leftCircle).unbind();
                $(settings.leftCircle).click(clickLeft);
            });
            changeComments();
        }


        init();
        $(settings.leftCircle).click(clickLeft);
        $(settings.rightCircle).click(clickRight);
    }

    $.SlideAnimate = $.fn.SlideAnimate = SlideAnimate;

}

)(jQuery);