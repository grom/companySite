﻿(
function($) {
    var container = ".slideshow";
    var slideId = "slide";
    var timer = "10000"; //ms

    function SlideShow() {
        var slideCount = "3";
        var previous = ".circle_left";
        var next = ".circle_right";
        var currentIndex = getCurrentIndex();
        var currentAnimated;

        init();

        function init() {
            $(previous).bind("click", movePrevious);
            $(next).bind("click", movePrevious);
            //autoMation();
        }

        function movePrevious() {
            currentIndex = getCurrentIndex();
            move((currentIndex - 1) % slideCount);

        }
        function moveNext() {
            currentIndex = getCurrentIndex();
            move((currentIndex + 1) % slideCount);
        }

        function move(index) {
            $(container).children().removeClass("show");
            $(container).children().css("display", "none");
            $($(container).children().get(index - 1)).fadeIn("slow").addClass("show");
            //            if ($(next).css("left") == "-180px") {
            //                $(previous).animate({ left: "-180px", top: "380px" }, 1000);
            //                $(next).animate({ left: "510px", top: "50px" }, 1000);
            //            } else {
            //                $(next).animate({ left: "-180px", top: "380px" }, 1000);
            //                $(previous).animate({ left: "510px", top: "50px" }, 1000);
            //            }
        }

        function autoMation() {
            currentAnimated = setInterval(moveNext, timer);
        }

        function getCurrentIndex() {
            var currentElem = $(container).children(".show").attr('id')
            return currentElem.substring(currentElem.length - 1);
        }

    }
    $(SlideShow);

}

)(jQuery);