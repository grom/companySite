﻿(
function($) {
    var mainMenuSele = "#nav .menu";

    function Menu() {
        $(mainMenuSele).each(function(index, element) {
            $(element).bind("mouseover", function() {
                $(element).children("ul").css("visibility", "visible");
                $(element).children("ul").css("z-index", "10");
            });
            $(element).bind("mouseout", function(e) {
                var $submenu = $(this).children("ul");
                if (e.pageX > $submenu.offset().left && e.pageX < $submenu.offset().left + $submenu.width()
                && e.pageY > $submenu.offset().top && +e.pageY < $submenu.offset().top + $submenu.height()) {
                    $submenu.bind("mouseout", function() { $(this).css("visibility", "hidden") });
                }
                else {
                    $(element).children("ul").css("visibility", "hidden");
                }

            });

        });
    }
    $(Menu);
}

)(jQuery);