<%@ include file="/jsp/include/head.jsp" %>
	<style type="text/css">
	#content #leftBar{
		position:relative;
		width:200px;
		float:left;padding-top:20px;padding-left:85px;padding-right:20px;height:auto;
	}
	#content #rightContent{
		position:relative;
		margin-left:300px;
		padding-top:10px;padding-left:10px;
	}
	#content #rightContent div p{
		padding-left:10px;
	}
	#content #rightContent div{
		border-bottom:#FFFFFF dashed 1px;
	}
	#content #leftBar div{
		width:200px;
		padding-top:5px;padding-left:15px;
		font-size:12px;
		border-bottom:#FFFFFF dotted 1px;
	}
	#contentTitle{
		background-color:#00CC99;
		height:20px;
		padding-top:3px;
		font-family:Georgia, "Times New Roman", Times, serif;
		font-size:14px;
		font-weight:bold;
	}
	</style>
	<script type="text/javascript">
	$(function(){
		$("a").each(function(){
			$(this).click(function(){
				alert($(this).attr("href"));
				if($(this).attr("href").indexOf("#",0)<0){
					return;
				}
				window.location.href=window.location.href.substring(0, window.location.href.indexOf("#", 0))+$(this).attr("href");
				return false; 
			});
		});
	});
	</script>
</head>
<body>
	<jsp:include page="/jsp/include/slidbar.jsp"></jsp:include>
<!-- insert your content here -->
        <div id="content" class="slideshow" style="height:2500px;">
            <div id="leftBar">
            	<div style="background-color:#009966;height:20px;font-size:15px;">Contact US</div>
                <div><a href="#1">ShangHai FanYue Logistics company</a></div>
                <div><a href="#2">LiuZhou branch company</a></div>
                <div><a href="#3">BengBu branch company</a></div>
                <div><a href="#4">NanJing branch office</a></div>
                <div><a href="#5">WeiFang branch office</a></div>
                <div><a href="#6">ZhenJiang branch office</a></div>
                <div><a href="#7">BeiJing branch office</a></div>
                <div><a href="#8">XiAn branch office</a></div>
                <div><a href="#9">ChengDu branch office</a></div>
                <div><a href="#10">GuangZhou branch office</a></div>
                <div><a href="#11">ZhengZhou branch office</a></div>
                <div><a href="#12">KunShan branch office</a></div>
                <div><a href="#13">XiaMen branch office</a></div>
                <div><a href="#14">TianJin branch office</a></div>
                <div><a href="#15">WuHan branch office</a></div>
            </div>
            <div id="rightContent">
            	<div id="1">
                	<p id="contentTitle">ShangHai FanYue Logistics company</p>
                	<p>Address:458 Fushan Road, Pudong New Area, Shanghai, with No. 11 TongSheng Building, Block K.</p>
					<p>Zip Code:200122</p>
					<p>Phone:021-58207117 to the extension </p>
					<p>Fax:021-58315989 </p>
					<p>Contact:Miss Zhang</p>
					<p>E-mail:company@fanyue.cn </p>
                </div>
                <div id="2">
                	<p id="contentTitle">LiuZhou branch company</p>
                	<p>Address:Liu Tai Road No. 1, next to Liugong Road Machinery Company, JinXing car repair plant</p>
					<p>Zip Code:545007 </p>
					<p>Phone:0772-3886809 </p>
					<p>Phone:0772-3886875 0772-3886876 　</p>
					<p>Fax:0772-3886893-801 </p>
					<p>Contact:Mr. Jiang </p>
					<p>Cel:13597063279</p>
                </div>
                <div id="3">
                	<p id="contentTitle">BengBu branch Company</p>
                	<p>Phone:0552-2865001 </p>
					<p>Fax:0552-2865002 </p>
					<p>Contact:Mr, Guo </p>
					<p>Cel:15205526305 </p>
                </div>
                <div id="4">
                	<p id="contentTitle">NanJing branch office</p>
                	<p>Address:Friendship Road , Qixia District No. 118 , NanJing</p>
					<p>Zip Code:210046 </p>
					<p>Fax:025-85593108 </p>
					<p>Contact:Mr, Zhang </p>
					<P>Cel:13951752695</P>
                </div>
                <div id="5">
                	<p id="contentTitle">WeiFang branch office</p>
                	<p>Address:East Weifang Minsheng Street, WeiChai quarters, Unit 6 Road, Building 9, Room 401</p>
					<p>Zip Code:261041 </p>
					<p>Phone/Fax:0536-2255230 </p>
					<p>Contact:Mr, Jiang </p>
					<p>Cel:13780804671 </p>
                </div>
                <div id="6">
                	<p id="contentTitle"> ZhenJiang branch office</p>
                	<p>Address:Zhufang Road, Sanmao District, Unit 2, Building 5, Room 404, ZhenJiang city, JiangSu province  </p>
					<p>Zip code:212005 </p>
					<p>Phone/Tax:0511-85634719 </p>
					<p>Contact: Mr, Zhu </p>
					<p>Cel:15952901237 </p>
                </div>
                <div id="7">
                	<p id="contentTitle"> BeiJing branch office</p>
                	<p>Address:BeiJing, Double Bridge Road, Chaoyang District, concrete mixing materials database victory </p>
					<p>Contact:Miss Zhang </p>
					<p>Cel:13693026024 </p>
                </div>
                <div id="8">
                	<p id="contentTitle"> XiAn branch office</p>
                	<p>Address:No. 223, Xi'an fenghe Road</p>
					<p>Phone:029-84621917 </p>
					<p>Fax:029-84613197 </p>
					<p>Contact:Mr, Qian </p>
					<p>Cel:15891449298 </p>
                </div>
                <div id="9">
                	<p id="contentTitle"> ChengDu branch office</p>
                	<p>Address:ChengDu ChengHua District Longtan Town Tree Village TongChuang engineering machinery limited company</p>
					<p>Phone:028-66756690 </p>
					<p>Contact:QianHua </p>
					<p>Cel:15908182170 </p>
                </div>
                <div id="10">
                	<p id="contentTitle">  GuangZhou branch office</p>
                	<p>Address:Guangzhou Baiyun District Road No. 47, 0-8 West Island library</p>
					<p>Contact:Mr, Jiang	</p>
					<p>Cel:13580445918 </p>
                </div>
                <div id="11">
                	<p id="contentTitle">  ZhongZhou branch office</p>
                	<p>Address:The liberation of South Road, Zhengzhou City, No 405,Room 303, </p>
					<p>Contact:Mr, Li </p>
					<p>Cel:13083817775 </p>
                </div>
                <div id="12">
                	<p id="contentTitle">  KunShan branch office</p>
                	<p>Address:Kunshan, Jiangsu Province, No 70 MinXin Road,  Room 102 </p>
					<p>Contact:Mr, Luo </p>
					<p>Cel:13805124427 </p>
                </div>
                <div id="13">
                	<p id="contentTitle">  XiaMen branch office</p>
                	<p>Address:Jimei District, Xiamen, Fujian Province, JiMei Town, Wangzhuang No. 227 , Room 518, fourth floor </p>
					<p>Zip Code:361023 </p>
					<p>Phone/Fax:0592-6311539 </p>
					<p>Contact:Mr, Shen </p>
					<p>Cel:18602109891 </p>
                </div>
                <div id="14">
                	<p id="contentTitle"> TianJin branch office</p>
                	<p>Contact:Mr, Wang</p>
					<p>Cel:18631209891 </p>
                </div>
                <div id="15">
                	<p id="contentTitle"> WuHan branch office</p>
                	<p>Contact:Mr, Zhou </p>
					<p>Cel:13971124668 </p>
                </div>
            </div>
        </div>
<%@ include file="/jsp/include/foot.jsp" %>