<%@ include file="/jsp/include/head.jsp" %>
	<style type="text/css">
	#title{
		margin-left:100px;
		padding-top:20px;
		padding-bottom:10px;
		font-size:14px;
		font-weight:bold;
		border-bottom:#FFFFFF 1px dotted;
	}
	#introduction{
		margin-left:100px;
		padding-top:10px;
		padding-right:50px;
	}
	#introduction p{
		text-indent:2em;
		font-size:13px;
	}
	</style>
</head>
<body>
	<jsp:include page="/jsp/include/slidbar.jsp"></jsp:include>
	<div id="content" class="slideshow" style="height:500px;">
		<div id="title">Services>>Struts</div>
         <div id="introduction">
          	<div id="introduction">
          		<img alt="" src="<%=request.getContextPath() %>/static/content/struts.jpg" />
      		</div>
         </div>
    </div>
<%@ include file="/jsp/include/foot.jsp" %>