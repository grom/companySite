<%@ include file="/jsp/include/head.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<style type="text/css">
	#title{
		background:transparent;
		margin-left:100px;
		padding-top:20px;
		padding-bottom:10px;
		font-size:14px;
		font-weight:bold;
		border-bottom:#FFFFFF 1px dotted;
	}
	#introduction{
		margin-left:100px;
		padding-top:10px;
		padding-right:50px;
	}
	#introduction p{
		
		font-size:13px;
	}
	</style>
</head>
<body>
	<jsp:include page="/jsp/include/slidbar.jsp"></jsp:include>
	<div id="content" class="slideshow" style="height:500px;">
		<div id="title">Services>>Storage</div>
         <div id="introduction">
          	<p style="font-size: 20px;font-weight: bold;">Resources</p>
			More than 40,000 sq.m. storage area.<br>
			<p style="font-size: 20px;font-weight: bold;">Services</p>
				<span style="font-weight: bold;">Simple product Storage</span><br>
				•	大型货架仓储（以托盘货物为主）<br>
				•	小型货架仓储（备件，小件货物）<br>
				•	平仓货物仓储<br>
				<span style="font-weight: bold;">Special product Storage</span><br>
				•	恒温恒湿货物仓储<br>
			<p style="font-size: 20px;font-weight: bold;">仓储增值服务</p>
			<span style="font-weight: bold;">VMI服务</span><br>
				•	帮助客户实现“零库存”管理<br>
				•	流通加工：产品分拣、加工、包装、重新包装、贴标签<br>
				•	信息服务：库存信息处理、条码技术、系统跟踪、数据交换<br>
         </div>
    </div>
<%@ include file="/jsp/include/foot.jsp" %>