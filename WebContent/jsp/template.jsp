<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="java.util.Map"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">  
<head> 
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />  
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery.js"></script>
<style type="text/css">
*{padding:0;margin:0;text-align:center;}
body{
	background-attachment: scroll;
	background-image: url(<%=request.getContextPath() %>/static/i/main_bg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	text-align:center;
	font-size:12px;
	font-family: "宋体", Arial;
}
a{
	font:bold 12px Verdana;
	color:#007153;
	text-decoration:none;
}
#wrapper{
	background-image: url(<%=request.getContextPath() %>/static/i/bgbg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	width:971px;
	height:700px;
	margin:0 auto;
}
#header{
	text-align:left;
	padding-left:50px;
	font-size:40px;
	color:#E7EBEA;
	height:75px;
}
#header a{
	color:#E7EBEA;
	font:12px normal;
}
#logo{
	display:block;
	position:relative;
	padding-top:20px;
	text-align:left;
}
#navigator{
	position:relative;
	font:bold 14px Verdana;
	text-align:right;
	padding-left:100px;
}
#navigator ul{
	list-style-image:none;
	list-style-type:none;
}
#navigator ul li{
	display:block;
	float:left;
	line-height:21px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background:url(<%=request.getContextPath() %>/static/i/nav1-line.gif) no-repeat 0px 0px;
	height:21px;
	width:70px;
}
#navigator ul li div{
	position:relative;
	display:none;
	margin-top:8px;
	width:700px;
	text-align:left;
	height:35px;
	z-index:10;
}
#navigator ul li:hover div{
	position:relative;
	display:block;
	margin-left:-15px;
	z-index:10;
}
#navigator ul li div span{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_bg.jpg) repeat-x 0px 0px;
	padding-left:5px;
	padding-right:5px;
	padding-top:3px;
	margin:0;
	float:left;
	height:40px;
}
#navigator ul li div span a{
	display:block;
	background:url(<%=request.getContextPath() %>/static/i/sub_menu_normal.jpg) no-repeat;
	width:110px;
	height:30px;
	padding-top:5px;
}
.begin{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_lbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
.end{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_rbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
#navigator ul li:hover{
	display:block;
	float:left;
	line-height:29px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background-color:#007153;
	height:21px;
}
#navigator ul li:hover a{
	color:#E7EBEA;
}
#first{
	background:url(<%=request.getContextPath() %>/static/i/nav1-left.gif) no-repeat 0px 0px;
}
#blank{
	position:relative;
	margin-top:30px;
	width:971px;
	//height:17px;
	background-color:#E7EBEA;
	padding-top:10px;
	text-align:left;
}
#content{
	position:relative;
	width:961px;
	height:400px;
	background-color:#E7EBEA;
	border-top:1px solid;
	padding-top:10px;
	padding-left:10px;
	text-align:left;
}
#leftBanner{
	position:relative;
	float:left;
	width:194px;
	text-align:left;
	padding-left:5px;
}
#realContent{
	position:relative;
	text-align:left;
	padding-left:210px;
	padding-right:15px;
	padding-top:5px;
	color:black;
}
#links{
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/topb.jpg) no-repeat 0px 0px;
	background-color:#FFF;
	height:200px;
}
#links a{
	display:block;
	color:black;
	text-align:left;
	padding-left:15px;
	padding-top:-5px;
	font-weight:normal;
	background:url(<%=request.getContextPath() %>/static/i/nav-arrow-03.gif) no-repeat 0px 0px;
	margin-top:8px;
}
#links a:hover{
	text-decoration:underline;
}
#links span{
	display:block;
	color:#FFF;
	font-size:16px ;
	font-weight:bold;
	text-align:left;
	padding-left:15px;
	padding-top:5px;
	height:20px;
}
#title{
	font-size:14px;
	font-weight:bold;
	padding-top:30px;
	padding-bottom:15px;
	border-bottom:1px dotted ;
}
#footer{
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/templatemo_bg.jpg) repeat ;
}
</style>
<script type="text/javascript">
</script>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<span id="logo">凡越物流</span>
		<div align="left" style="font-size: 10px; color: #FFF;position:relative;left:400px;top:-55px;">
            <p align="left" style="font-size:10px;color:#FFF">
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=zh-cn">中文</a> | 
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=eng">Eng</a>
			</p>
		</div>
	</div>
	<div id="navigator">
		<ul>
			<li class="first">
				<a href="<%=request.getContextPath() %>/DirectionAction/gotoHomePage.do">首页</a>
			</li>
			<%
				List<RootMenu> rootMenus = (List<RootMenu>)request.getAttribute("rootMenus");
	    		Map<String, List<SecondMenu>> childMenus = (Map<String, List<SecondMenu>>)request.getAttribute("childMenus");
	    		int length = rootMenus.size();
	    		for(int i = 0 ; i < length ; i++){
	    	%>
	    	<li class="first">
				<a href="#"><%=rootMenus.get(i).getName() %></a>
				<div>
					<tt class="begin"></tt>
					<%
						List<SecondMenu> thisChildren = childMenus.get(rootMenus.get(i).getID());
						int ch_len = thisChildren.size();
	            		for(int l = 0; l < ch_len ; l++){
	            	%>
		            	<span>
		            		<a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>">
		            			<%=thisChildren.get(l).getName() %>
		            		</a>
		            	</span> 
	            	<%
	            		}
					%>
					<tt class="end"></tt>
				</div>
			</li>
	    	<%
	    		}
			%>
			<li class="first">
				<a href="#">关于我们</a>
			</li>
		</ul>
	</div>
	<div id="blank">
		<img src="<%=request.getContextPath() %>/static/i/banner.jpg" style="width:971px;"/>
	</div>
	<div id="content">
		<div id="leftBanner">
			<div id="links">
				<span>新闻中心</span>
				<a href="#">新闻1</a>
				<a href="#">新闻2</a>
				
			</div>
			<img src="<%=request.getContextPath() %>/static/i/kefu.jpg"/>
		</div>
		<div id="realContent">
			<span>您现在的位置：首页 >> 新闻中心 >> 通知公告 >> 关于恢复佳怡物流网上下单业务的通知</span>
			<div id="title">
				关于恢复佳怡物流网上下单业务的通知
			</div>
			<div id="article">
				
			</div>
		</div>
	</div>
	<div id="footer">
		copyright@fanyue
	</div>
</div>
</body>
</html>