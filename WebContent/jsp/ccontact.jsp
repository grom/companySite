<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="java.util.Map"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">  
<head> 
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />  
<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery.js"></script>
<style type="text/css">
*{padding:0;margin:0;text-align:center;}
body{
	background-attachment: scroll;
	background-image: url(<%=request.getContextPath() %>/static/i/main_bg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	text-align:center;
	font-size:12px;
	font-family: "宋体", Arial;
}
a{
	font:bold 12px Verdana;
	color:#007153;
	text-decoration:none;
}
#wrapper{
	background-image: url(<%=request.getContextPath() %>/static/i/bgbg.jpg);
	background-repeat: repeat-x;
	background-position: left top;
	background-color: #E7EBEA;
	width:971px;
	height:700px;
	margin:0 auto;
}
#header{
	text-align:left;
	padding-left:50px;
	font-size:40px;
	color:#E7EBEA;
	height:75px;
}
#header a{
	color:#E7EBEA;
	font:12px normal;
}
#logo{
	display:block;
	position:relative;
	padding-top:20px;
	text-align:left;
}
#navigator{
	position:relative;
	font:bold 14px Verdana;
	text-align:right;
	padding-left:100px;
}
#navigator ul{
	list-style-image:none;
	list-style-type:none;
}
#navigator ul li{
	display:block;
	float:left;
	line-height:21px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background:url(<%=request.getContextPath() %>/static/i/nav1-line.gif) no-repeat 0px 0px;
	height:21px;
	width:70px;
}
#navigator ul li div{
	position:relative;
	display:none;
	margin-top:8px;
	width:700px;
	text-align:left;
	height:35px;
	z-index:10;
}
#navigator ul li:hover div{
	position:relative;
	display:block;
	margin-left:-15px;
	z-index:10;
}
#navigator ul li div span{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_bg.jpg) repeat-x 0px 0px;
	padding-left:5px;
	padding-right:5px;
	padding-top:3px;
	margin:0;
	float:left;
	height:40px;
}
#navigator ul li div span a{
	display:block;
	background:url(<%=request.getContextPath() %>/static/i/sub_menu_normal.jpg) no-repeat;
	width:110px;
	height:30px;
	padding-top:5px;
}
.begin{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_lbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
.end{
	display:block;
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/doc_rbg.jpg) no-repeat 0px 0px;
	padding:0;
	margin:0;
	float:left;
	height:35px;
	width:15px;
}
#navigator ul li:hover{
	display:block;
	float:left;
	line-height:29px;
	padding-left:18px;
	padding-right:18px;
	padding-top:8px;
	background-color:#007153;
	height:21px;
}
#navigator ul li:hover a{
	color:#E7EBEA;
}
#first{
	background:url(<%=request.getContextPath() %>/static/i/nav1-left.gif) no-repeat 0px 0px;
}
#blank{
	position:relative;
	margin-top:30px;
	width:971px;
	background-color:#E7EBEA;
	padding-top:10px;
	text-align:left;
}
#content{
	position:relative;
	width:961px;
	height:1320px;
	background-color:#E7EBEA;
	border-top:1px solid;
	padding-top:10px;
	padding-left:10px;
	text-align:left;
}
#leftBanner{
	position:relative;
	float:left;
	width:194px;
	text-align:left;
	padding-left:5px;
}
#realContent{
	position:relative;
	text-align:left;
	padding-left:210px;
	padding-right:15px;
	padding-top:5px;
	color:black;
	border-bottom:dotted 1px;
	padding-bottom:20px;
}
#links{
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/topb.jpg) no-repeat 0px 0px;
	background-color:#FFF;
	height:200px;
}
#links a{
	display:block;
	color:black;
	text-align:left;
	padding-left:15px;
	padding-top:-5px;
	font-weight:normal;
	background:url(<%=request.getContextPath() %>/static/i/nav-arrow-03.gif) no-repeat 0px 0px;
	margin-top:8px;
}
#links a:hover{
	text-decoration:underline;
}
#links span{
	display:block;
	color:#FFF;
	font-size:16px ;
	font-weight:bold;
	text-align:left;
	padding-left:15px;
	padding-top:5px;
	height:20px;
}
#title{
	font-size:14px;
	font-weight:bold;
	padding-top:30px;
	padding-bottom:15px;
	border-bottom:1px dotted ;
	text-align: left;
	padding-left:20px;
}
#introduction{
	padding-top:10px;
	padding-right:50px;
	text-align: left;
}
#introduction p{
	font-size:13px;
	text-align: left;
	padding-left:50px;
	margin-top:10px;
	padding-bottom:10px;
	display:block;
}
#footer{
	position:relative;
	background:url(<%=request.getContextPath() %>/static/i/templatemo_bg.jpg) repeat ;
}
#foot_blank{
	background:#E7EBEA;
	height:50px;
}

#content #leftBar{
		position:relative;
		width:200px;
		float:left;
		padding-top:20px;
		padding-left:0px;padding-right:20px;height:auto;
	}
	#content #rightContent{
		position:relative;
		margin-left:250px;
		padding-top:10px;padding-left:10px;
	}
	#content #rightContent div p{
		padding-left:10px;
	}
	#content #rightContent div{
		border-bottom:#FFFFFF dashed 1px;
	}
	#content #leftBar div{
		width:200px;
		padding-top:5px;padding-left:15px;
		font-size:12px;
		border-bottom:#FFFFFF dotted 1px;
	}
	#contentTitle{
		background-color:#00CC99;
		height:20px;
		padding-top:3px;
		font-family:Georgia, "Times New Roman", Times, serif;
		font-size:14px;
		font-weight:bold;
	}
</style>
<script type="text/javascript">
</script>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<span id="logo">凡越物流</span>
		<div align="left" style="font-size: 10px; color: #FFF;position:relative;left:400px;top:-55px;">
            <p align="left" style="font-size:10px;color:#FFF">
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=zh-cn">中文</a> | 
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=eng">Eng</a>
			</p>
		</div>
	</div>
	<div id="navigator">
		<ul>
			<li class="first">
				<a href="<%=request.getContextPath() %>/DirectionAction/gotoHomePage.do">首页</a>
			</li>
			<%
				List<RootMenu> rootMenus = (List<RootMenu>)request.getAttribute("rootMenus");
	    		Map<String, List<SecondMenu>> childMenus = (Map<String, List<SecondMenu>>)request.getAttribute("childMenus");
	    		int length = rootMenus.size();
	    		for(int i = 0 ; i < length ; i++){
	    	%>
	    	<li class="first">
				<a href="#"><%=rootMenus.get(i).getName() %></a>
				<div>
					<tt class="begin"></tt>
					<%
						List<SecondMenu> thisChildren = childMenus.get(rootMenus.get(i).getID());
						int ch_len = thisChildren.size();
	            		for(int l = 0; l < ch_len ; l++){
	            	%>
		            	<span>
		            		<a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>">
		            			<%=thisChildren.get(l).getName() %>
		            		</a>
		            	</span> 
	            	<%
	            		}
					%>
					<tt class="end"></tt>
				</div>
			</li>
	    	<%
	    		}
			%>
			<li class="first">
				<a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=OTHERS">联系我们</a>
			</li>
		</ul>
	</div>
	<div id="blank">
		<!-- img src="<%=request.getContextPath() %>/static/i/cangchu_banner.jpg" style="width:971px;"/-->
	</div>
	<div id="content">
            <div id="leftBar">
            	<div style="background-color:#009966;height:20px;font-size:15px;">联系我们</div>
                <div><a href="#1">上海凡越物流有限公司</a></div>
                <div><a href="#2">柳州分公司</a></div>
                <div><a href="#3">蚌埠分公司</a></div>
                <div><a href="#4">南京办事处</a></div>
                <div><a href="#5">潍坊办事处</a></div>
                <div><a href="#6">镇江办事处</a></div>
                <div><a href="#7">北京办事处</a></div>
                <div><a href="#8">西安办事处</a></div>
                <div><a href="#9">成都办事处</a></div>
                <div><a href="#10">广州办事处</a></div>
                <div><a href="#11">郑州办事处</a></div>
                <div><a href="#12">昆山办事处</a></div>
                <div><a href="#13">厦门办事处</a></div>
                <div><a href="#14">天津办事处</a></div>
                <div><a href="#15">武汉办事处</a></div>
            </div>
            <div id="rightContent">
            	<div id="1">
                	<p id="contentTitle">上海凡越物流有限公司</p>
                	<p>地址：上海市浦东新区福山路458号同盛大厦11楼K座</p>
					<p>邮编：200122</p>
					<p>电话：021-58207117转各分机 </p>
					<p>传真：021-58315989 </p>
					<p>联系人：张小姐</p>
					<p>E-mail：company@fanyue.cn </p>
                </div>
                <div id="2">
                	<p id="contentTitle">柳州分公司</p>
                	<p>地址：柳太路1号柳工路面机械公司旁金星汽车维修厂内</p>
					<p>邮编：545007 </p>
					<p>柳工配件办：0772-3886809 </p>
					<p>电话：0772-3886875 0772-3886876 　</p>
					<p>传真：0772-3886893-801 </p>
					<p>联系人：蒋先生 </p>
					<p>手机：13597063279</p>
                </div>
                <div id="3">
                	<p id="contentTitle">蚌埠分公司</p>
                	<p>电话：0552-2865001 </p>
					<p>传真：0552-2865002 </p>
					<p>联系人：郭先生 </p>
					<p>手机：15205526305 </p>
                </div>
                <div id="4">
                	<p id="contentTitle">南京办事处</p>
                	<p>地址：南京市栖霞区友谊路118号 </p>
					<p>邮编：210046 </p>
					<p>传真：025-85593108 </p>
					<p>联系人：张先生 </p>
					<P>手机：13951752695</P>
                </div>
                <div id="5">
                	<p id="contentTitle">潍坊办事处</p>
                	<p>地址：潍坊市民生东街潍柴宿舍6路9号楼一单元401室 </p>
					<p>邮编：261041 </p>
					<p>电话/传真：0536-2255230 </p>
					<p>联系人：蒋先生 </p>
					<p>手机：13780804671 </p>
                </div>
                <div id="6">
                	<p id="contentTitle"> 镇江办事处</p>
                	<p>地址：江苏省镇江市朱方路三茅小区2区5栋404室 </p>
					<p>邮编：212005 </p>
					<p>电话/传真：0511-85634719 </p>
					<p>联系人：祝先生 </p>
					<p>手机：15952901237 </p>
                </div>
                <div id="7">
                	<p id="contentTitle"> 北京办事处</p>
                	<p>地址：北京市朝阳区双桥中路胜利混凝土搅拌建材库内 </p>
					<p>联系人：张小姐 </p>
					<p>手机：13693026024 </p>
                </div>
                <div id="8">
                	<p id="contentTitle"> 西安办事处</p>
                	<p>地址：西安市丰禾路223号</p>
					<p>电话：029-84621917 </p>
					<p>传真：029-84613197 </p>
					<p>联系人：钱先生 </p>
					<p>手机：15891449298 </p>
                </div>
                <div id="9">
                	<p id="contentTitle"> 成都办事处</p>
                	<p>地址：成都市成华区龙潭乡树村同创工程机械有限公司 </p>
					<p>电话：028-66756690 </p>
					<p>联系人：钱华 </p>
					<p>手机：15908182170 </p>
                </div>
                <div id="10">
                	<p id="contentTitle">  广州办事处</p>
                	<p>地址：广州市白云区西洲北路47号0-8库 </p>
					<p>联系人：蒋先生	</p>
					<p>手机：13580445918 </p>
                </div>
                <div id="11">
                	<p id="contentTitle">  郑州办事处</p>
                	<p>地址：郑州市解放南路405号303室 </p>
					<p>联系人：李先生 </p>
					<p>手机：13083817775 </p>
                </div>
                <div id="12">
                	<p id="contentTitle">  昆山办事处</p>
                	<p>地址：江苏省昆山市民新路70号102室 </p>
					<p>联系人：骆先生 </p>
					<p>手机：13805124427 </p>
                </div>
                <div id="13">
                	<p id="contentTitle">  厦门办事处</p>
                	<p>地址：福建省厦门市集美区灌口镇王庄227号四楼518室 </p>
					<p>邮编：361023 </p>
					<p>电话/传真：0592-6311539 </p>
					<p>联系人：沈先生 </p>
					<p>手机：18602109891 </p>
                </div>
                <div id="14">
                	<p id="contentTitle"> 天津办事处</p>
                	<p>联系人：王先生</p>
					<p>手机：18631209891 </p>
                </div>
                <div id="15">
                	<p id="contentTitle"> 武汉办事处</p>
                	<p>联系人：周先生 </p>
					<p>手机：13971124668 </p>
                </div>
        </div>
	</div>
	<div id="footer">
		copyright@fanyue
	</div>
	<div id="foot_blank">
		
	</div>
</div>
</body>
</html>