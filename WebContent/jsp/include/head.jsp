<%@page import="com.grom.service.CustomerService"%>
<%@page import="com.grom.po.Customer"%>
<%@page import="java.util.Map"%>
<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
    <title>凡越物流</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/customer.css" />
    <script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery.js"></script>