<%@page import="com.grom.service.CustomerService"%>
<%@page import="com.grom.po.Customer"%>
<%@page import="java.util.Map"%>
<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<div style="position: absolute;font-size:22px;margin-top:0px;margin-left:150px;color:#009900">
		<a href="<%=request.getContextPath() %>"><img src="<%=request.getContextPath() %>/static/i/logo.jpg" style="width:200px;height:60px;border:none"/></a>
		<div align="left" style="font-size: 10px; color: #003300;position:relative;left:720px;top:-70px;">
            <p align="left" style="font-size:10px;color:#003300">
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=zh-cn">中文</a> | 
			<a href="<%=request.getContextPath() %>/DirectionAction/changeLan.do?lan=eng">Eng</a>
			</p>
		</div>
	</div>
	<!-- 头部导航部分从这里开始 -->
    <div id="wrapper">
        <div id="header">
            <div id="nav">
            	<%
            		List<RootMenu> rootMenus = (List<RootMenu>)request.getAttribute("rootMenus");
            		Map<String, List<SecondMenu>> childMenus = (Map<String, List<SecondMenu>>)request.getAttribute("childMenus");
            		int length = rootMenus.size();
            		for(int i = 0 ; i < length ; i++){
            			if( i == 0 ){
            				%>
            				<div class="menu first">
            				<%
            			}else if(i== length -1){
            				%>
            				<div class="menu last">
            				<%
            			}else{
            				%>
            				<div class="menu">
            				<%
            			}
            				%>
                    			<a href="#"><%=rootMenus.get(i).getName() %></a>
			                    <ul class="submenu">
			                    	<%
			                    		List<SecondMenu> thisChildren = childMenus.get(rootMenus.get(i).getID());
			                    		int ch_len = thisChildren.size();
			                    		for(int l = 0; l < ch_len ; l++){
			                    			if(l==0){
			                    				%>
			                    				<li class="first"><a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>"><%=thisChildren.get(l).getName() %></a></li>
			                    				<%
			                    			}else if(l==ch_len-1 ){
			                    				%>
			                    				<li class="last"><a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>"><%=thisChildren.get(l).getName() %></a></li>
			                    				<%
			                    			}else{
			                    				%>
			                    				<li><a href="<%=request.getContextPath() %>/DirectionAction/gotoChildPage.do?id=<%=thisChildren.get(l).getID() %>"><%=thisChildren.get(l).getName() %></a></li>
			                    				<%
			                    			}
			                    		}
			                    	%>
			                    </ul>
			                </div>
            				<%
            		}
            	%>
            </div>
        </div>
        <script type="text/javascript" src="<%=request.getContextPath() %>/static/js/menu.js"></script>
        <!-- 头部导航部分结束 -->
 		<div id="topline">
        </div>