<%@ include file="/jsp/include/head.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<style type="text/css">
	#title{
		background:transparent;
		margin-left:100px;
		padding-top:20px;
		padding-bottom:10px;
		font-size:14px;
		font-weight:bold;
		border-bottom:#FFFFFF 1px dotted;
	}
	#introduction{
		margin-left:100px;
		padding-top:10px;
		padding-right:50px;
	}
	#introduction p{
		text-indent:2em;
		font-size:13px;
	}
	#picContent{
		margin-left:150px;
		padding-top:10px; 
		padding-right:50px;
	}
	</style>
</head>
<body>
	<jsp:include page="/jsp/include/slidbar.jsp"></jsp:include>
	<div id="content" class="slideshow" style="height:700px;">
		<div id="title">Services>>Delivery&Distribution</div>
		<div id="picContent">
			<img alt="" src="<%=request.getContextPath() %>/static/content/fanyue.jpg" />
		</div>
        <div id="introduction">
			<p style="font-size: 20px;font-weight: bold;">Resources</p>
				We have more than 800 cars which owned ourselves and our collaborators, they will be ready for all 7*24 Hrs. <br>
				Thanks to the GPS and some other kinds of tools, with the experiences on the management and control method, we can use these cars which belongs to our collaborators very quickly.<br>
				We use the KPI to control our work, and the level will be better and better.<br>
			<p style="font-size: 20px;font-weight: bold;">Services Area</p>
			<span style="font-weight: bold;">Land transportation</span><br>
				•	全国主要城市间的公路干线运输<br>
				•	市内及其周边地区的配送（门到门服务）<br>
				•	仓库/工厂，仓库/仓库间的短驳运输<br>
				•	根据客户的特殊要求提供定制化的服务<br>
			<span style="font-weight: bold;">国内空运</span><br>
				•	主要城市始发，到全国各个省市的航空运输<br>
				•	配合我司的车辆资源，货物可以送达至暂无机场的城市<br>
			<span style="font-weight: bold;">国内铁路</span><br>
				•	利用铁路的资源优势，结合优质的自有车辆站点配送，提升覆盖范围和运作质量。<br>
			<p style="font-size: 20px;font-weight: bold;">生产配送</p>
         		根据客户生产计划，将所需部件按时按量送达生产线上。<br>
         </div>
    </div>
<%@ include file="/jsp/include/foot.jsp" %>