﻿<%@page import="com.grom.service.CustomerService"%>
<%@page import="com.grom.po.Customer"%>
<%@page import="java.util.Map"%>
<%@page import="com.grom.bo.SecondMenu"%>
<%@page import="com.grom.bo.RootMenu"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
	
    <title>上海凡越</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/static/css/customer.css" />
    <script type="text/javascript" src="<%=request.getContextPath() %>/static/js/jquery.js"></script>
    <style type="text/css">
    	#comments p{
    		margin: 0;
    		color: yellow;
    	}
    	#comments a{
    		font-size: 14px;
    		text-align: left;
    	}
    	#urlShow{
    		position: absolute;
    		margin-top: 270px;
    		margin-left: 750px;
    		z-index: 100;
    		font-size: 20px;
    	}
    	#urlShow a{
    		font-size: 15px;
    	}
    	#urlShow a:HOVER{
    		font-size: 18px;
    		color: green;
    		font-weight: bold;
    	}
    </style>
	
</head>
<body>
	<jsp:include page="/jsp/include/slidbar.jsp"></jsp:include>
    <div id="comments" style="position: absolute;font-size:22px;margin-top:120px;margin-left:150px;width: 200px;">
    	<p>物流资源系统规划服务 : </P><a href='<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=guihua'>点击查看详细</a>
	</div>
	<div id="urlShow">
		<a href="<%=request.getContextPath() %>/DirectionAction/gotoService.do?id=guihua">查看详细</a>
	</div>
        <div id="content" class="slideshow">
        	
            <div id="slide1" class="main_content show">
                <div class="circle_left">
                    <a>
                        <img src="static/i/circle_left.png" />
                    </a>
                </div>
                <div class="circle_center">
                    <a>
                        <img src="static/i/circle_center.png" />
                    </a>
                </div>
                <div class="circle_right">
                    <a>
                        <img src="static/i/circle_right.png" />
                    </a>
                </div>
            </div>
        </div>
<div class="messagewrap">
	<div id="title">成功案例</div>
	<ul>
		<%
			List<Customer> allCustomers = CustomerService.instance.getAllCustomers();
			for(Customer cus : allCustomers){
				%>
					<li>
						<img width="40px" height="20px" src="<%=request.getContextPath() %>/static/pic/<%=cus.getLogo() %>"/>
						<a href="<%=cus.getUrl() %>">
						<%
							if(session.getAttribute("lan").equals("eng")){
								out.print(cus.getEname());
							}else{
								out.print(cus.getCname());
							}
						%>
						</a>
					</li>
				<%
			}
		%>
	</ul>
	<div class="bottomcover">
		<!–为了符合w3c要求不能有空标签的要求，此处可选择写一个&nbsp;–>
	</div>
</div>

    <script type="text/javascript" src="<%=request.getContextPath() %>/static/js/menu.js"></script>

	<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/animate.js"></script>

    <script type="text/javascript">
        $.SlideAnimate({urlStart:"<%=request.getContextPath() %>",productImags: ["<%=request.getContextPath() %>/static/i/canchu.png", "<%=request.getContextPath() %>/static/i/gongyinglian.png", "<%=request.getContextPath() %>/static/i/guihua.png",
                                       "<%=request.getContextPath() %>/static/i/yunshu.png", "<%=request.getContextPath() %>/static/i/xinxihua.png", "<%=request.getContextPath() %>/static/i/huodai.png"]});
    </script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/static/js/customer.js"></script>
	<div>
		<hr>
		law : <a href="#">The detail of the law will be hold on the company</a>
		copy rights @ Fanyue shanghai company<br>
		contact: dwbin1984@GMAIL.com<br>
		phone : 021-05*********<br>
		<a href="login.html">manager entry</a>
		<br>
	</div>

</body>
</html>
