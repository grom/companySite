package com.grom.listener;

import java.sql.Connection;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.grom.conn.ConnectionCreator;
import com.grom.conn.DatabaseInit;
import com.holder.log.ConsoleLog;

/**
 * Application Lifecycle Listener implementation class DatabaseInitListener
 * 
 */
public class DatabaseInitListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public DatabaseInitListener() {

	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		ConsoleLog.debug("now will check the database!");
		Connection conn = ConnectionCreator.getDefaultConn();
		if (!DatabaseInit.isDatabaseInited(conn)) {
			DatabaseInit.initTopMenu(conn);
			DatabaseInit.initChildMenu(conn);
			DatabaseInit.initCustomer(conn);
		}
		ConsoleLog.debug("databse init finished!");
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {

	}

}
