package com.grom.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.grom.bo.RootMenu;
import com.grom.bo.SecondMenu;
import com.grom.po.ChildMenu;
import com.grom.po.TopMenu;
import com.grom.service.ChildMenuService;
import com.grom.service.TopMenuService;
import com.holder.log.ConsoleLog;

public abstract class MenuHolder {

	public static final String ZH_CN = Constants.LAN_ZH_CN;
	public static final String ENG = Constants.LAN_ENG;
	/**
	 * 数据库中有更新的时候将这个标志修改为true
	 */
	public static boolean needRefresh = false;

	static Map<String, List<RootMenu>> rootMenus = new HashMap<String, List<RootMenu>>();

	static Map<String, Map<String, List<SecondMenu>>> secondMenus = new HashMap<String, Map<String, List<SecondMenu>>>();

	/**
	 * 这个方法用于从数据库中抽取所有相应的数据并初始化到相应的数组
	 */
	static void init() {
		ConsoleLog.debug("now will initialize the lazy load of the menu!");
		rootMenus.clear();
		secondMenus.clear();
		buildRootMenus();
		buildSecondMenus();
		ConsoleLog.debug("init finished!");
	}

	private static void buildRootMenus() {
		List<TopMenu> topMenus = TopMenuService.instance.getAllTopMenus();
		List<RootMenu> zh_cn_menus = rootMenus.get(ZH_CN);
		if (zh_cn_menus == null) {
			zh_cn_menus = new ArrayList<RootMenu>();
			rootMenus.put(ZH_CN, zh_cn_menus);
		}
		List<RootMenu> eng_menus = rootMenus.get(ENG);
		if (eng_menus == null) {
			eng_menus = new ArrayList<RootMenu>();
			rootMenus.put(ENG, eng_menus);
		}
		for (TopMenu menu : topMenus) {
			RootMenu zh_cn = new RootMenu();
			zh_cn.setID(menu.getID());
			zh_cn.setName(menu.getCname());
			zh_cn_menus.add(zh_cn);
			RootMenu eng = new RootMenu();
			eng.setID(menu.getID());
			eng.setName(menu.getEname());
			eng_menus.add(eng);
		}
	}

	private static void buildSecondMenus() {
		List<TopMenu> topMenus = TopMenuService.instance.getAllTopMenus();
		Map<String, List<SecondMenu>> zh_cn_mensu = secondMenus.get(ZH_CN);
		if (zh_cn_mensu == null) {
			zh_cn_mensu = new HashMap<String, List<SecondMenu>>();
			secondMenus.put(ZH_CN, zh_cn_mensu);
		}
		Map<String, List<SecondMenu>> eng_mensu = secondMenus.get(ENG);
		if (eng_mensu == null) {
			eng_mensu = new HashMap<String, List<SecondMenu>>();
			secondMenus.put(ENG, eng_mensu);
		}
		for (TopMenu menu : topMenus) {
			String sql = "select * from ChildMenu where PARENTID = '" + menu.getID() + "'";
			List<ChildMenu> childMenus = ChildMenuService.instance.selectByCondition(sql);
			List<SecondMenu> zh_cn_seconds = new ArrayList<SecondMenu>();
			List<SecondMenu> eng_seconds = new ArrayList<SecondMenu>();
			zh_cn_mensu.put(menu.getID(), zh_cn_seconds);
			eng_mensu.put(menu.getID(), eng_seconds);
			for (ChildMenu child : childMenus) {
				SecondMenu zh_cn = new SecondMenu();
				zh_cn.setID(child.getID());
				zh_cn.setName(child.getCname());
				zh_cn.setAction(child.getCaction());// 这个不一定需要
				zh_cn_seconds.add(zh_cn);
				SecondMenu eng = new SecondMenu();
				eng.setID(child.getID());
				eng.setName(child.getEname());
				eng.setAction(child.getEaction());// 这个不一定需要
				eng_seconds.add(eng);
			}
		}
	}

	public static List<RootMenu> getRootMenu(String location) {
		List<RootMenu> result = rootMenus.get(location);
		if (needRefresh || result == null) {
			init();
			needRefresh = false;
			result = rootMenus.get(location);
		}
		return result;
	}

	public static Map<String, List<SecondMenu>> getSecondMenu(String location) {
		Map<String, List<SecondMenu>> result = secondMenus.get(location);
		if (needRefresh || result == null) {
			init();
			needRefresh = false;
			result = secondMenus.get(location);
		}
		return result;
	}
}
