package com.grom.util;

import java.util.HashMap;
import java.util.Map;

public interface Constants {
	String COOKIE_KEY = "lan";
	
	String LAN_ZH_CN="zh-cn";
	
	String LAN_ENG = "eng";
	
	String KEY_PAGE_SIZE = "page.size";

	String MODULE_ROOT = "ROOT";

	String KEY_USER_SESSION = "CURRENTUSER";
	/**
	 * the config file's name of the column defination
	 */
	String COLUMN_CONFIG_FILE_NAME = "column-config.xml";
	/**
	 * the key used in the config file which marked the name part
	 */
	String COLUMN_NAME_KEY = "column-name";
	/**
	 * the key used to mark the property name with the column name
	 */
	String PROPERTY_NAME_KEY = "property-name";
	/**
	 * used to mark the position of the property in the file
	 */
	String COLUMN_POSITION_KEY = "column-position";

	String COLUMN_BASIC_KEY = "column-item";

	String PARSER_TYPE_KEY = "parser_type";
	/**
	 * default sheet index
	 */
	int DEFAULT_SHEET_INDEX = 0;
	/**
	 * the line nums used to store the statistics
	 */
	int GATHER_PART_LINE_NUMS = 11;
	/**
	 * the first row number
	 */
	int FIRST_ROW_NUM = 0;
	/**
	 * no column defination in the config-column.xml then return this value
	 */
	int NONCOLUMN_DEFINATION = -1;
	/**
	 * the regular patter used to match the numbers
	 */
	String NUM_REGEX = "[0-9]d*";
	/**
	 * the date type used in the file name of the log
	 */
	String DATE_FORMAT_LOGNAME = "yyyyMMdd";

	int ONEWEEKTIME = 8 * 24 * 3600 * 1000;

	int CASEID_LENGTH = 8;
	/**
	 * 
	 */
	String PREFIX_CASEID = "CASE";

	@SuppressWarnings("serial")
	Map<Integer, String> NUM_TO_CHAR = new HashMap<Integer, String>() {
		{
			for (int i = 0; i < 26; i++) {
				put(i, String.valueOf((char) ('A' + i)));
			}
		}
	};

	@SuppressWarnings("serial")
	Map<String, Integer> CHAR_TO_NUM = new HashMap<String, Integer>() {
		{
			for (int i = 0; i < 26; i++) {
				put(String.valueOf((char) ('A' + i)), i);
			}
		}
	};
}
