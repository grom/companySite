package com.grom.util;

import java.util.UUID;

public abstract class IDCreator {
	/**
	 * create one ID by UUID
	 * 
	 * @return
	 */
	public static String getSingleId() {
		return UUID.randomUUID().toString();
	}
}
