package com.grom.service;

import java.util.List;

import com.grom.dao.TopMenuDao;
import com.grom.po.TopMenu;

public interface TopMenuService {
	TopMenuService instance = new TopMenuService() {

		public List<TopMenu> getAllTopMenus() {
			return TopMenuDao.instance.getAllTopMenus();
		}

		public void add(TopMenu object) {
			TopMenuDao.instance.add(object);
		}

		public void update(TopMenu object) {
			TopMenuDao.instance.update(object);
		}

		public TopMenu getTopMenuById(String ID) {
			return TopMenuDao.instance.getTopMenuById(ID);
		}

		public void delete(TopMenu object) {
			TopMenuDao.instance.delete(object);
		}

		public List<TopMenu> selectByCondition(String sql) {
			return TopMenuDao.instance.selectByCondition(sql);
		}

	};

	List<TopMenu> getAllTopMenus();

	List<TopMenu> selectByCondition(String sql);

	TopMenu getTopMenuById(String ID);

	void add(TopMenu object);

	void delete(TopMenu object);

	void update(TopMenu object);

}
