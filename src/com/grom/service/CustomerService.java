package com.grom.service;

import java.util.List;

import com.grom.dao.CustomerDao;
import com.grom.po.Customer;

public interface CustomerService {
	CustomerService instance = new CustomerService() {

		public List<Customer> getAllCustomers() {
			return CustomerDao.instance.getAllCustomers();
		}

		public void add(Customer object) {
			CustomerDao.instance.add(object);
		}

		public void update(Customer object) {
			CustomerDao.instance.update(object);
		}

		public Customer getCustomerById(String ID) {
			return CustomerDao.instance.getCustomerById(ID);
		}

		public void delete(Customer object) {
			CustomerDao.instance.delete(object);
		}

		public List<Customer> selectByCondition(String sql) {
			return CustomerDao.instance.selectByCondition(sql);
		}

	};

	List<Customer> getAllCustomers();

	List<Customer> selectByCondition(String sql);

	Customer getCustomerById(String ID);

	void add(Customer object);

	void delete(Customer object);

	void update(Customer object);

}
