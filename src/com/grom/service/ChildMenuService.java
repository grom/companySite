package com.grom.service;

import java.util.List;

import com.grom.dao.ChildMenuDao;
import com.grom.po.ChildMenu;

public interface ChildMenuService {
	ChildMenuService instance = new ChildMenuService() {

		public List<ChildMenu> getAllChildMenus() {
			return ChildMenuDao.instance.getAllChildMenus();
		}

		public void add(ChildMenu object) {
			ChildMenuDao.instance.add(object);
		}

		public void update(ChildMenu object) {
			ChildMenuDao.instance.update(object);
		}

		public ChildMenu getChildMenuById(String ID) {
			return ChildMenuDao.instance.getChildMenuById(ID);
		}

		public void delete(ChildMenu object) {
			ChildMenuDao.instance.delete(object);
		}

		public List<ChildMenu> selectByCondition(String sql) {
			return ChildMenuDao.instance.selectByCondition(sql);
		}

	};

	List<ChildMenu> getAllChildMenus();

	List<ChildMenu> selectByCondition(String sql);

	ChildMenu getChildMenuById(String ID);

	void add(ChildMenu object);

	void delete(ChildMenu object);

	void update(ChildMenu object);

}
