package com.grom.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.dispacher.context.ContextHolder;
import com.grom.po.Customer;
import com.grom.service.CustomerService;
import com.grom.util.JsonUtil;
import com.holder.log.ConsoleLog;

public class CustomerAction {
	public void gotoList() {
		List<Customer> allCustomers = CustomerService.instance.getAllCustomers();
		ContextHolder.setRequestAttribute("allCustomers", allCustomers);
		ContextHolder.forward("/WEB-INF/jsp/Customer/update.jsp");
	}

	public void doSave() {
		Customer object = (Customer) ContextHolder.initFromTheRequest(Customer.class);
		if (object.getID() == null || object.getID().trim().equals("")) {
			CustomerService.instance.add(object);
		} else {
			CustomerService.instance.update(object);
		}
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/CustomerAction/gotoList.do");
	}

	public void getDetail() {
		String id = ContextHolder.findInstance().getRequest().getParameter("itemid");
		Customer object = CustomerService.instance.getCustomerById(id);
		String json = JsonUtil.parseToJson(object);
		ConsoleLog.debug("| CustomerAction | getDetail | json | " + json);
		HttpServletResponse response = ContextHolder.findInstance().getResponse();
		try {
			PrintWriter out = response.getWriter();
			out.write("Customer=" + json);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void doDelete() {
		String id = ContextHolder.findInstance().getRequest().getParameter("deleteId");
		System.out.println("| CustomerAction | doDelete | id | " + id);
		Customer object = CustomerService.instance.getCustomerById(id);
		CustomerService.instance.delete(object);
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/CustomerAction/gotoList.do");
	}

	public void conditionSelect() {
		String clothType_s = ContextHolder.findInstance().getRequest().getParameter("clothType_s");
		String brandName_s = ContextHolder.findInstance().getRequest().getParameter("brandName_s");
		String sql = "select * from Customer where clothType like '%" + clothType_s + "%' and brandName like '%"
				+ brandName_s + "%'";
		List<Customer> allCustomers = CustomerService.instance.selectByCondition(sql);
		ContextHolder.setRequestAttribute("allCustomers", allCustomers);
		ContextHolder.forward("/WEB-INF/jsp/Customer/update.jsp");
	}
}
