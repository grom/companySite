package com.grom.action;

import javax.servlet.http.Cookie;

import com.dispacher.context.ContextHolder;
import com.dispacher.context.RequestContext;
import com.dispacher.context.ResponseContext;
import com.grom.po.ChildMenu;
import com.grom.service.ChildMenuService;
import com.grom.util.Constants;
import com.grom.util.MenuHolder;

public class DirectionAction {
	public void gotoHomePage() {
		Cookie[] cookies = ContextHolder.findInstance().getRequest()
				.getCookies();
		String lan = Constants.LAN_ZH_CN;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(Constants.COOKIE_KEY)) {
					lan = cookie.getValue();
				}
			}
		} else {
			Cookie cookie = new Cookie(Constants.COOKIE_KEY, lan);
			ResponseContext.getCommonResponse().addCookie(cookie);
		}
		RequestContext.setSessionAttribute(Constants.COOKIE_KEY, lan);
		ResponseContext.setReturnAttr("rootMenus", MenuHolder.getRootMenu(lan));
		ResponseContext.setReturnAttr("childMenus",
				MenuHolder.getSecondMenu(lan));
		ResponseContext.forword("/index.jsp");
	}

	public void changeLan() {
		String lan = ContextHolder.findInstance().getRequest()
				.getParameter(Constants.COOKIE_KEY);
		Cookie cookie = new Cookie(Constants.COOKIE_KEY, lan);
		ContextHolder.findInstance().getResponse().addCookie(cookie);
		ContextHolder.getCurSession().setAttribute(Constants.COOKIE_KEY, lan);
		ContextHolder.setRequestAttribute("rootMenus",
				MenuHolder.getRootMenu(lan));
		ContextHolder.setRequestAttribute("childMenus",
				MenuHolder.getSecondMenu(lan));
		ContextHolder.forward("/index.jsp");
	}

	public void login() {
		ContextHolder.forward("/frame.jsp");
	}

	public void gotoChildPage() {
		String id = ContextHolder.findInstance().getRequest()
				.getParameter("id");
		ChildMenu child = ChildMenuService.instance.getChildMenuById(id);
		String lan = getLan();
		ContextHolder.setRequestAttribute("rootMenus",
				MenuHolder.getRootMenu(lan));
		ContextHolder.setRequestAttribute("childMenus",
				MenuHolder.getSecondMenu(lan));
		if (child == null || child.getCaction() == null
				|| child.getEaction() == null
				|| child.getCaction().indexOf(".") < 0) {
			ContextHolder.forward("/jsp/todo/todo.jsp");
			return;
		}
		if (lan.equals(Constants.LAN_ZH_CN)) {
			ContextHolder.forward("/jsp/" + child.getCaction());
		} else {
			ContextHolder.forward("/jsp/" + child.getEaction());
		}
	}

	public void gotoService() {
		String id = ContextHolder.findInstance().getRequest()
				.getParameter("id");
		ChildMenu child = ChildMenuService.instance.getChildMenuById(id);
		String lan = getLan();
		ContextHolder.setRequestAttribute("rootMenus",
				MenuHolder.getRootMenu(lan));
		ContextHolder.setRequestAttribute("childMenus",
				MenuHolder.getSecondMenu(lan));
		if (lan.equals(Constants.LAN_ZH_CN)) {
			ContextHolder.forward("/jsp/" + child.getCaction());
		} else {
			ContextHolder.forward("/jsp/" + child.getEaction());
		}
	}

	public String getLan() {
		String lan = (String) ContextHolder.getCurSession().getAttribute(
				Constants.COOKIE_KEY);
		if (lan == null) {
			lan = Constants.LAN_ZH_CN;
			ContextHolder.getCurSession().setAttribute(Constants.COOKIE_KEY,
					lan);
			Cookie cookie = new Cookie(Constants.COOKIE_KEY, lan);
			ContextHolder.findInstance().getResponse().addCookie(cookie);
		}
		return lan;
	}
}
