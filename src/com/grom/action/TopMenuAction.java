package com.grom.action;

import java.util.List;

import com.dispacher.context.ContextHolder;
import com.grom.po.TopMenu;
import com.grom.service.TopMenuService;
import com.grom.util.MenuHolder;

public class TopMenuAction {
	public void gotoList() {
		List<TopMenu> allTopMenus = TopMenuService.instance.getAllTopMenus();
		ContextHolder.setRequestAttribute("allTopMenus", allTopMenus);
		ContextHolder.forward("/WEB-INF/jsp/TopMenu/update.jsp");
	}

	public void doSave() {
		TopMenu object = (TopMenu) ContextHolder.initFromTheRequest(TopMenu.class);
		if (object.getID() == null || object.getID().trim().equals("")) {
			TopMenuService.instance.add(object);
		} else {
			TopMenuService.instance.update(object);
		}
		MenuHolder.needRefresh = true;
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/TopMenuAction/gotoList.do");
	}

	public TopMenu getDetail() {
		String id = ContextHolder.findInstance().getRequest().getParameter("itemid");
		TopMenu object = TopMenuService.instance.getTopMenuById(id);
		return object;
	}

	public void doDelete() {
		String id = ContextHolder.findInstance().getRequest().getParameter("deleteId");
		System.out.println("| TopMenuAction | doDelete | id | " + id);
		TopMenu object = TopMenuService.instance.getTopMenuById(id);
		TopMenuService.instance.delete(object);
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/TopMenuAction/gotoList.do");
	}

	public void conditionSelect() {
		String clothType_s = ContextHolder.findInstance().getRequest().getParameter("clothType_s");
		String brandName_s = ContextHolder.findInstance().getRequest().getParameter("brandName_s");
		String sql = "select * from TopMenu where clothType like '%" + clothType_s + "%' and brandName like '%"
				+ brandName_s + "%'";
		List<TopMenu> allTopMenus = TopMenuService.instance.selectByCondition(sql);
		ContextHolder.setRequestAttribute("allTopMenus", allTopMenus);
		ContextHolder.forward("/WEB-INF/jsp/TopMenu/update.jsp");
	}
}
