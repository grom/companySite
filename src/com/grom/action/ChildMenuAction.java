package com.grom.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.dispacher.context.ContextHolder;
import com.grom.po.ChildMenu;
import com.grom.service.ChildMenuService;
import com.grom.util.JsonUtil;
import com.grom.util.MenuHolder;
import com.holder.log.ConsoleLog;

public class ChildMenuAction {
	public void gotoList() {
		List<ChildMenu> allChildMenus = ChildMenuService.instance.getAllChildMenus();
		ContextHolder.setRequestAttribute("allChildMenus", allChildMenus);
		ContextHolder.forward("/WEB-INF/jsp/ChildMenu/update.jsp");
	}

	public void doSave() {
		ChildMenu object = (ChildMenu) ContextHolder.initFromTheRequest(ChildMenu.class);
		if (object.getID() == null || object.getID().trim().equals("")) {
			ChildMenuService.instance.add(object);
		} else {
			ChildMenuService.instance.update(object);
		}
		MenuHolder.needRefresh = true;
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/ChildMenuAction/gotoList.do");
	}

	public void getDetail() {
		String id = ContextHolder.findInstance().getRequest().getParameter("itemid");
		ChildMenu object = ChildMenuService.instance.getChildMenuById(id);
		String json = JsonUtil.parseToJson(object);
		ConsoleLog.debug("| ChildMenuAction | getDetail | json | " + json);
		HttpServletResponse response = ContextHolder.findInstance().getResponse();
		try {
			PrintWriter out = response.getWriter();
			out.write("ChildMenu=" + json);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void doDelete() {
		String id = ContextHolder.findInstance().getRequest().getParameter("deleteId");
		System.out.println("| ChildMenuAction | doDelete | id | " + id);
		ChildMenu object = ChildMenuService.instance.getChildMenuById(id);
		ChildMenuService.instance.delete(object);
		ContextHolder.sendRedirect(ContextHolder.findInstance().getContextUri() + "/ChildMenuAction/gotoList.do");
	}

	public void conditionSelect() {
		String clothType_s = ContextHolder.findInstance().getRequest().getParameter("clothType_s");
		String brandName_s = ContextHolder.findInstance().getRequest().getParameter("brandName_s");
		String sql = "select * from ChildMenu where clothType like '%" + clothType_s + "%' and brandName like '%"
				+ brandName_s + "%'";
		List<ChildMenu> allChildMenus = ChildMenuService.instance.selectByCondition(sql);
		ContextHolder.setRequestAttribute("allChildMenus", allChildMenus);
		ContextHolder.forward("/WEB-INF/jsp/ChildMenu/update.jsp");
	}
}
