package com.grom.action;

import java.io.File;

import com.dispacher.context.ContextHolder;
import com.dispacher.upload.UploadHolder;
import com.holder.log.ConsoleLog;

public class ImportAction {
	public void gotoImport() {
		ContextHolder.forward("/WEB-INF/jsp/util/htmlupload.jsp");
	}

	public void gotoImporte() {
		ContextHolder.forward("/WEB-INF/jsp/util/ehtmlupload.jsp");
	}

	public void gotoImportPic() {
		ContextHolder.forward("/WEB-INF/jsp/util/picupload.jsp");
	}

	public void doImport() {
		String path = ContextHolder.getCurSession().getServletContext().getRealPath("/jsp");
		ConsoleLog.debug("now the path : " + path);
		File file = UploadHolder.saveFileTo("C:/");
		file.renameTo(new File(path + "/" + file.getName()));
		System.out.println(file.getAbsolutePath());
		ContextHolder.setRequestAttribute("returnPath", file.getName());
		ContextHolder.forward("/WEB-INF/jsp/util/success.jsp");
	}

	public void doImporte() {
		String path = ContextHolder.getCurSession().getServletContext().getRealPath("/jsp");
		ConsoleLog.debug("now the path : " + path);
		File file = UploadHolder.saveFileTo("C:/");
		file.renameTo(new File(path + "/" + file.getName()));
		System.out.println(file.getAbsolutePath());
		ContextHolder.setRequestAttribute("returnPath", file.getName());
		ContextHolder.forward("/WEB-INF/jsp/util/esuccess.jsp");
	}

	public void doImportpic() {
		String path = ContextHolder.getCurSession().getServletContext().getRealPath("/static/pic");
		ConsoleLog.debug("now the path : " + path);
		File file = UploadHolder.saveFileTo("C:/");
		file.renameTo(new File(path + "/" + file.getName()));
		System.out.println(file.getAbsolutePath());
		ContextHolder.setRequestAttribute("returnPath", file.getName());
		ContextHolder.forward("/WEB-INF/jsp/util/picsuccess.jsp");
	}

}
