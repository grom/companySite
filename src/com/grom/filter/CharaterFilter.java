package com.grom.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dispacher.config.GlobalConfig;

/**
 * Servlet Filter implementation class CharaterFilter
 */
public class CharaterFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public CharaterFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		request.setCharacterEncoding(GlobalConfig.getProperty("app.encoding"));
		response.setCharacterEncoding(GlobalConfig.getProperty("app.encoding"));
		if (request.getProtocol().compareTo("HTTP/1.0") == 0)
			response.setHeader("Pragma", "no-cache");
		if (request.getProtocol().compareTo("HTTP/1.1") == 0)
			response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
