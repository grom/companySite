package com.grom.bo;

/**
 * 该类用于界面显示
 * @author Administrator
 * 
 */
public class RootMenu {
	private String ID;
	private String name;

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
