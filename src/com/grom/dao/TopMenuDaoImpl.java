package com.grom.dao;

import java.util.List;

import com.grom.po.TopMenu;
import com.grom.util.IDCreator;
import com.holder.DBContextHolder;
import com.holder.DBUTil;

class TopMenuDaoImpl implements TopMenuDao {

	public List<TopMenu> getAllTopMenus() {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<TopMenu> result = DBUTil.select(TopMenu.class);
		return result;
	}

	public void add(TopMenu object) {
		object.setID(IDCreator.getSingleId());
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.save(object);
	}

	public void update(TopMenu object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.update(object);
	}

	public TopMenu getTopMenuById(String ID) {
		String sql = "select * from TopMenu where id = '" + ID + "'";
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<TopMenu> result = DBUTil.getResult(sql, TopMenu.class);
		return result.get(0);
	}

	public void delete(TopMenu object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.delete(object);
	}

	public List<TopMenu> selectByCondition(String sql) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<TopMenu> result = DBUTil.getResult(sql, TopMenu.class);
		return result;
	}

}
