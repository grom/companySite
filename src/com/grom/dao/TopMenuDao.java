package com.grom.dao;

import java.util.List;

import com.grom.po.TopMenu;

public interface TopMenuDao {
	TopMenuDao instance = new TopMenuDaoImpl();

	List<TopMenu> getAllTopMenus();
	
	List<TopMenu> selectByCondition(String sql);
	
	TopMenu getTopMenuById(String ID);
	
	void add(TopMenu object);
	
	void update(TopMenu object);
	
	void delete(TopMenu object);
}