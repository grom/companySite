package com.grom.dao;

import java.util.List;

import com.grom.po.Customer;

public interface CustomerDao {
	CustomerDao instance = new CustomerDaoImpl();

	List<Customer> getAllCustomers();
	
	List<Customer> selectByCondition(String sql);
	
	Customer getCustomerById(String ID);
	
	void add(Customer object);
	
	void update(Customer object);
	
	void delete(Customer object);
}