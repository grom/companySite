package com.grom.dao;

import java.util.List;

import com.grom.po.Customer;
import com.grom.util.IDCreator;
import com.holder.DBContextHolder;
import com.holder.DBUTil;

class CustomerDaoImpl implements CustomerDao {

	public List<Customer> getAllCustomers() {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<Customer> result = DBUTil.select(Customer.class);
		return result;
	}

	public void add(Customer object) {
		object.setID(IDCreator.getSingleId());
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.save(object);
	}

	public void update(Customer object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.update(object);
	}

	public Customer getCustomerById(String ID) {
		String sql = "select * from Customer where id = '" + ID + "'";
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<Customer> result = DBUTil.getResult(sql, Customer.class);
		return result.get(0);
	}

	public void delete(Customer object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.delete(object);
	}

	public List<Customer> selectByCondition(String sql) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<Customer> result = DBUTil.getResult(sql, Customer.class);
		return result;
	}

}
