package com.grom.dao;

import java.util.List;

import com.grom.po.ChildMenu;
import com.grom.util.IDCreator;
import com.holder.DBContextHolder;
import com.holder.DBUTil;

class ChildMenuDaoImpl implements ChildMenuDao {

	public List<ChildMenu> getAllChildMenus() {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<ChildMenu> result = DBUTil.select(ChildMenu.class);
		return result;
	}

	public void add(ChildMenu object) {
		object.setID(IDCreator.getSingleId());
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.save(object);
	}

	public void update(ChildMenu object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.update(object);
	}

	public ChildMenu getChildMenuById(String ID) {
		String sql = "select * from ChildMenu where id = '" + ID + "'";
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<ChildMenu> result = DBUTil.getResult(sql, ChildMenu.class);
		return result.get(0);
	}

	public void delete(ChildMenu object) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		DBUTil.delete(object);
	}

	public List<ChildMenu> selectByCondition(String sql) {
		DBContextHolder.setContextConnection(DatabaseDao.getConnection());
		List<ChildMenu> result = DBUTil.getResult(sql, ChildMenu.class);
		return result;
	}

}
