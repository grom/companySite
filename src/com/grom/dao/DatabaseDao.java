package com.grom.dao;

import java.sql.Connection;
import com.grom.conn.ConnectionCreator;

public class DatabaseDao {
	public static Connection getConnection() {
		return ConnectionCreator.getDefaultConn();
	}

	public static void main(String[] args) {
		System.out.println(getConnection());
	}
}
