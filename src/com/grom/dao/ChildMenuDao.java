package com.grom.dao;

import java.util.List;

import com.grom.po.ChildMenu;

public interface ChildMenuDao {
	ChildMenuDao instance = new ChildMenuDaoImpl();

	List<ChildMenu> getAllChildMenus();
	
	List<ChildMenu> selectByCondition(String sql);
	
	ChildMenu getChildMenuById(String ID);
	
	void add(ChildMenu object);
	
	void update(ChildMenu object);
	
	void delete(ChildMenu object);
}