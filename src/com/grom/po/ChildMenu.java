package com.grom.po;
/**
*author @Grom
*/
public class ChildMenu{
	private String ID;
	private String Cname;
	private String Ename;
	private String Caction;
	private String Eaction;
	private String PARENTID;

	public void setCname(String Cname){
		this.Cname=Cname;
	}
	public String getCname(){
		return this.Cname;
	}
	public void setCaction(String Caction){
		this.Caction=Caction;
	}
	public String getCaction(){
		return this.Caction;
	}
	public void setEaction(String Eaction){
		this.Eaction=Eaction;
	}
	public String getEaction(){
		return this.Eaction;
	}
	public void setEname(String Ename){
		this.Ename=Ename;
	}
	public String getEname(){
		return this.Ename;
	}
	public void setID(String ID){
		this.ID=ID;
	}
	public String getID(){
		return this.ID;
	}
	public void setPARENTID(String PARENTID){
		this.PARENTID=PARENTID;
	}
	public String getPARENTID(){
		return this.PARENTID;
	}
}
