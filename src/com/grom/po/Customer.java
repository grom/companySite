package com.grom.po;
/**
*author @Grom
*/
public class Customer{
	private String ID;
	private String Cname;
	private String Ename;
	private String url;
	private String logo;

	public void setCname(String Cname){
		this.Cname=Cname;
	}
	public String getCname(){
		return this.Cname;
	}
	public void setLogo(String logo){
		this.logo=logo;
	}
	public String getLogo(){
		return this.logo;
	}
	public void setEname(String Ename){
		this.Ename=Ename;
	}
	public String getEname(){
		return this.Ename;
	}
	public void setID(String ID){
		this.ID=ID;
	}
	public String getID(){
		return this.ID;
	}
	public void setUrl(String url){
		this.url=url;
	}
	public String getUrl(){
		return this.url;
	}
}
