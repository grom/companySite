package com.grom.po;
/**
*author @Grom
*/
public class TopMenu{
	private String ID;
	private String Cname;
	private String Ename;

	public void setCname(String Cname){
		this.Cname=Cname;
	}
	public String getCname(){
		return this.Cname;
	}
	public void setEname(String Ename){
		this.Ename=Ename;
	}
	public String getEname(){
		return this.Ename;
	}
	public void setID(String ID){
		this.ID=ID;
	}
	public String getID(){
		return this.ID;
	}
}
