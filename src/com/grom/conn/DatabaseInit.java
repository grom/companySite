package com.grom.conn;

import java.sql.Connection;
import java.util.List;

import com.grom.po.TopMenu;
import com.holder.DBContextHolder;
import com.holder.DBUTil;

public abstract class DatabaseInit {

	public static boolean isDatabaseInited(Connection conn) {
		DBContextHolder.setContextConnection(conn);
		try {
			List<TopMenu> result = DBUTil.select(TopMenu.class);
			if (result == null || result.size() == 0) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void initTopMenu(Connection conn) {
		DBContextHolder.setContextConnection(conn);
		String sql = "drop table TopMenu;";
		DBUTil.update(sql);
		sql = "create table TopMenu"
				+ "(ID varchar(60) not null, Cname varchar(60) not null, Ename varchar(60) not null);";
		DBUTil.update(sql);
		sql = "insert into TopMenu(id, cname, ename) values('ABOUTUS','关于我们','About US');";
		DBUTil.update(sql);
		sql = "insert into TopMenu(id, cname, ename) values('DYNAMIC','公司动态','Dynamic');";
		DBUTil.update(sql);
		// sql =
		// "insert into TopMenu(id, cname, ename) values('SOLUTIONS','解决方案','Solutions');";
		// DBUTil.update(sql);
		sql = "insert into TopMenu(id, cname, ename) values('CONTACTUS','招贤纳士','Contact US');";
		DBUTil.update(sql);
	}

	public static void initChildMenu(Connection conn) {
		DBContextHolder.setContextConnection(conn);
		String sql = "drop table ChildMenu;";
		DBUTil.update(sql);
		sql = "create table ChildMenu"
				+ "(ID varchar(60) not null, Cname varchar(60) not null, Ename varchar(60) not null,"
				+ "parentId varchar(60) not null, caction varchar(60) not null, eaction varchar(60) not null);";
		DBUTil.update(sql);
		/** ABOUTUS start here. **/
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('COMPANY','公司简介','Company','ABOUTUS','ecompany.jsp','ccompany.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('HONOURS','企业荣誉','Honours','ABOUTUS','ehonour.jsp','chonour.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('HISTORY','发展历程','History','ABOUTUS','ehistory.jsp','chistory.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('STRUTS','组织架构','Struts','ABOUTUS','estruts.jsp','cstruts.jsp');";
		DBUTil.update(sql);
		/** Dynamic start here **/
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('NEWS','新闻资讯','News','DYNAMIC','eaction','caction');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('INDUSTRY','行业动态','Industry','DYNAMIC','eaction','caction');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('OTHERS','其他','Others','DYNAMIC','econtact.jsp','ccontact.jsp');";
		DBUTil.update(sql);
		/** 招贤纳士 start here **/
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('HEROS','人才理念','Talent concept','CONTACTUS','eheros.jsp','cheros.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('JOBS','职位招聘','Jobs','CONTACTUS','eaction','caction');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
				+ "values('COOPERATION','合作加盟','Cooperation','CONTACTUS','ecooperation.jsp','ccooperation.jsp');";
		DBUTil.update(sql);
		//Service 
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('cangchu','仓储','Storage','null','ecangchu.jsp','ccangchu.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('gongyinglian','供应链','gongyinglian','null','egongyinglian.jsp','cgongyinglian.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('guihua','规划','guihua','null','eguihua.jsp','cguihua.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('yunshu','运输','yunshu','null','eyunshu.jsp','cyunshu.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('xinxihua','信息化','xinxihua','null','exinxihua.jsp','cxinxihua.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('xinxihua','信息化','xinxihua','null','exinxihua.jsp','cxinxihua.jsp');";
		DBUTil.update(sql);
		sql = "insert into ChildMenu(id, cname, ename,parentId,eaction,caction) "
			+ "values('huodai','货代','huodai','null','ehuodai.jsp','chuodai.jsp');";
		DBUTil.update(sql);
	}

	public static void initCustomer(Connection conn) {
		DBContextHolder.setContextConnection(conn);
		String sql = "drop table customer;";
		DBUTil.update(sql);
		sql = "create table customer"
				+ "(ID varchar(60) not null, Cname varchar(60) not null, Ename varchar(60) not null,"
				+ "url varchar(60) not null, logo varchar(60) not null);";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
				+ "values('gxlg','广西柳工股份有限公司','GuangXi LiuGong Company','#','gzlg.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('SHZH','上海振华港机(集团)公司','ShangHaiGangHua Company','#','shzhgj.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('XNS','西诺迪斯食品','XiNuoSi','#','xns.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('SHCYJ','上海柴油机股份有限公司','ShangHai Diesel engine Company','#','shcyj.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('VOLVO','沃尔沃建筑设备中国','volvo','#','volvo.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('TOSHIBA','东芝照明显示系统（上海）有限公司','TOSHIBA','#','dzzm.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('Case','凯斯工程机械有限公司','Case','#','ks.jpg');";
		DBUTil.update(sql);
		sql = "insert into customer(id, cname, ename,url,logo) "
			+ "values('JESEY','杰西博工程机械(上海)有限公司','Jesey company','#','jx.jpg');";
		DBUTil.update(sql);
	}
}
